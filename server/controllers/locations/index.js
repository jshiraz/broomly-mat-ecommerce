const knex = require('../../lib/mysql');
const logger = require('../../lib/logger');

const locationsController = {};

/*
  locationController( name String )
*/
locationsController.getLocationByCity = (city) => {
  return knex.from('locations').where('location_name', city).limit(1)
    .then((location) => {
      return location;
    })
    .catch((err) => {
      return err;
    });
};

module.exports = locationsController;
