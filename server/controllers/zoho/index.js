const knex = require('../../lib/mysql');
const logger = require('../../lib/logger');

const zohoController = {};

/**
 * Grabs the current OAuth token for Zoho from the database and returns it.
 * @param {string} clientId - Used to find the correct OAuth token in the database.
 */
zohoController.getOAuthToken = (clientId) => {
  return knex('zoho').where('zoho_client_id', clientId).select('zoho_oauth_token')
    .then((oAuthToken) => {
      return oAuthToken[0].zoho_oauth_token;
    })
    .catch((err) => {
      throw err;
    });
};

/**
 * Grabs the refresh token associated with the given Client ID.
 * @param {string} clientId - Used to find the correct Refresh token in the database.
 */
zohoController.getRefreshToken = (clientId) => {
  return knex('zoho').where('zoho_client_id', clientId).select('zoho_refresh_token')
    .then((refreshToken) => {
      return refreshToken[0].zoho_refresh_token;
    })
    .catch((err) => {
      logger.error(err);
      throw err;
    });
};

zohoController.getLastRefresh = (clientId) => {
  return knex('zoho').where('zoho_client_id', clientId).select('last_refreshed')
    .then((lastRefresh) => {
      return lastRefresh[0];
    })
    .catch((err) => {
      throw err;
    });
};

/**
 * Updates the current OAuth Token with a fresh one.
 * @param {string} refreshToken Used to find the correct table even though there is only one.
 * @param {string} oAuthToken The new OAuth token to be used in the database.
 */
zohoController.updateOAuthToken = (clientId, oAuthToken) => {
  const currentTime = Date.now();

  return knex('zoho').where('zoho_client_id', clientId).update({
    zoho_oauth_token: oAuthToken,
    last_refreshed: currentTime,
  });
};

module.exports = zohoController;
