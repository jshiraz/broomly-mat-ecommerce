const axios = require('axios');
const logger = require('../logger');
const zohoController = require('../../controllers/zoho');

const zoho = {};

/**
 * Checks to see if the OAuth token in the database has expired.
 * @returns Returns the refreshInterval variable or 0.
 */
zoho.isOAuthTokenExpired = () => {
  return zohoController.getLastRefresh(process.env.ZOHO_CLIENT_ID)
    .then((lastRefresh) => {
      if (typeof lastRefresh !== 'undefined') {
        logger.debug('Last time OAuth Token was refreshed: ', Date(lastRefresh.last_refreshed).toString());
        const refreshInterval = (parseInt(lastRefresh.last_refreshed, 10) + 3595000) - Date.now();
        if (refreshInterval <= 0) {
          return 0;
        }
        return refreshInterval;
      }
      throw Error('No OAuth token found in database.');
    })
    .catch((err) => {
      logger.error(err);
      throw err;
    });
};

/**
 * Refreshes an access token for Zoho.
 * @param {string} refreshToken - Refresh token used by Zoho to create a new OAuth token.
 * @returns Promise
 */
zoho.refreshOAuthToken = (refreshToken) => {
  const baseUrl = 'https://accounts.zoho.com/oauth/v2/token?';
  const clientId = process.env.ZOHO_CLIENT_ID;
  const clientSecret = process.env.ZOHO_CLIENT_SECRET;

  return axios.post(`${baseUrl}refresh_token=${refreshToken}&client_id=${clientId}&client_secret=${clientSecret}&grant_type=refresh_token`)
    .then((refreshToken) => {
      if (typeof refreshToken.data.error === 'undefined') {
        return refreshToken.data;
      }
      throw Error(refreshToken.data.error);
    })
    .then((oAuthToken) => {
      setTimeout(() => {
        logger.info('Zoho OAuth Token will refresh in 59 minutes');
        zoho.refreshOAuthToken(refreshToken);
      }, 3590000);
      return zohoController.updateOAuthToken(process.env.ZOHO_CLIENT_ID, oAuthToken.access_token);
    })
    .catch((err) => {
      logger.error('refreshOAuthToken - updateOAuthToken: ', err);
      throw err;
    });
};

zoho.oAuthTokenRefreshInit = () => {
  // Check to see if OAuth token is expired
  zoho.isOAuthTokenExpired()
    .then((refreshInterval) => {
      logger.debug('Refresh Zoho OAuth Token in', refreshInterval);
      // Get the refresh token from the database.
      return zohoController.getRefreshToken(process.env.ZOHO_CLIENT_ID)
        .then((refreshToken) => {
          // Refresh the token
          setTimeout(() => {
            zoho.refreshOAuthToken(refreshToken);
          }, refreshInterval);
        })
        .catch((err) => {
          logger.error(err);
          throw err;
        });
    })
    .catch((err) => {
      logger.error('Catching error in oAuthTokenRefresh Init', err);
      throw err;
    });
};

/**
 * Upserts Lead information into Zoho.
 * @param {leadData} leadData - Information to be upserted to Zoho.
 */
zoho.upsertLead = (leadData) => {
  const baseUrl = process.env.ZOHO_DOMAIN.concat('/leads/upsert');
  return zohoController.getOAuthToken(process.env.ZOHO_CLIENT_ID)
    .then((oAuthToken) => {
      return axios.post(
        baseUrl, {
          data: [leadData],
        },
        {
          headers: {
            Authorization: `Zoho-oauthtoken ${oAuthToken}`,
          },
        },
      );
    })
    .then((upsertResponse) => {
      if (upsertResponse.data.status === 'error') {
        throw Error(upsertResponse.data[0]);
      }
      return upsertResponse.data;
    })
    .catch((err) => {
      throw err;
    });
};

zoho.getLeads = () => {
  const zohoUrl = process.env.ZOHO_DOMAIN.concat('/leads');
  return zohoController.getOAuthToken(process.env.ZOHO_CLIENT_ID)
    .then((oAuthToken) => {
      return axios.get(
        zohoUrl,
        {
          headers: {
            Authorization: `Zoho-oauthtoken ${oAuthToken}`,
          },
        },
      );
    })
    .then((upsertResponse) => {
      if (upsertResponse.data.status === 'error') {
        throw Error(upsertResponse.data[0]);
      }
      return upsertResponse.data;
    })
    .catch((err) => {
      throw err;
    });
};

zoho.convertLeadToContact = (leadId, userId) => {
  const zohoUrl = process.env.ZOHO_DOMAIN.concat(`/Leads/${leadId}/actions/convert`);
  return zohoController.getOAuthToken(process.env.ZOHO_CLIENT_ID)
    .then((oAuthToken) => {
      return axios.post(
        zohoUrl,
        { data: [{ assign_to: userId }] },
        { headers: { Authorization: `Zoho-oauthtoken ${oAuthToken}` } },
      );
    })
    .then((upsertResponse) => {
      if (upsertResponse.data.status === 'error') {
        throw Error(upsertResponse.data[0]);
      }
      return upsertResponse.data;
    })
    .catch((err) => {
      throw err;
    });
};

module.exports = zoho;
