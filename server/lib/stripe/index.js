const stripe = require('stripe')(process.env.STRIPE_SECRET);

const libStripe = {};

libStripe.createCustomerId = (token, email) => {
  if (!token || !email) {
    return new Promise((resolve) => {
      resolve(false);
    });
  }
  return stripe.customers.create({
    email,
    source: token,
  });
};

module.exports = libStripe;
