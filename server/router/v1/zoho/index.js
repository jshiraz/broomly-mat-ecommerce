const router = require('express').Router();
const zoho = require('../../../lib/zoho');
const logger = require('../../../lib/logger');
const stripe = require('../../../lib/stripe');

router.get('/lead', (req, res) => {
  const phone = req.query.phone || null;
  const email = req.query.email || null;
  zoho.getLeads()
    .then((leads) => {
      logger.debug(leads);
    })
    .catch((err) => {
      logger.error(err);
    });
});

router.post('/lead', (req, res) => {
  console.log(req.body)
  if (!req.body.hasOwnProperty('stripe')) {
    zoho.upsertLead(req.body.zoho)
      .then((response) => {
        logger.debug('Upsert Data', response.data);
        res.send('200').status(200);
      })
      .catch((err) => {
        logger.error('Error: ', err);
        res.sendStatus(500);
      });
    return true;
  }
  res.sendStatus(200);
  return false;
});

router.post('/contact', (req, res) => {
  stripe.createCustomerId(req.body.stripe.id, req.body.zoho.Email)
    .then((customerId) => {
      return zoho.upsertLead(Object.assign(req.body.zoho, { StripeId: customerId.id, Lead_Status: 'Sold - Enter into Stripe/Jobber' }));
    })
    .then((zohoContact) => {
      logger.debug('Upsert Lead', zohoContact.data[0]);
      res.sendStatus(201);
    })
    .catch((err) => {
      logger.error('Error: ', err);
      res.sendStatus(500);
    });
});

router.get('/oauth/callback', (req, res) => {
  console.log(req.query);
});

module.exports = router;
