const router = require('express').Router();
const location = require('./location');
const zoho = require('./zoho');

router.use('/location', location);
router.use('/zoho', zoho);

module.exports = router;
