const router = require('express').Router();
const logger = require('../../../lib/logger');
const locationController = require('../../../controllers/locations');

router.get('/', (req, res) => {
  locationController.getLocationByCity('austin')
    .then((location) => {
      if (!location) {
        console.log('Location doesnt exist in the DB');
        res.sendStatus(500);
        return null;
      }
      res.send(location[0]);
    });
});

module.exports = router;
