import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PhoneNumber = ({ phoneNumber }) => (
  <a href={`tel:+1${phoneNumber.locationPhoneNumber}`}>
    { phoneNumber.locationPhoneNumber }
  </a>
);

// Check if the Redux thunk has resolved with the phone number yet.
PhoneNumber.isPending = (phoneNumber) => {
  if (phoneNumber.status === 'PENDING') {
    return true;
  }
  return false;
};

// Generate the tel: link text that will go in the href.
PhoneNumber.getLinkText = (phoneNumber) => {
  if (!PhoneNumber.isPending) {
    return `tel:+1${phoneNumber.locationphoneNumber.replace(/-/g, '')}`;
  }
  return '';
};

PhoneNumber.propTypes = {
  phoneNumber: PropTypes.shape({
    locationPhoneNumber: PropTypes.string,
    status: PropTypes.string,
  }),
};

PhoneNumber.defaultProps = {
  phoneNumber: {
    locationPhoneNumber: '555-129-5384',
    status: 'PENDING',
  },
};

const mapStateToProps = (state) => {
  return { phoneNumber: state.routerReducer };
};

export default connect(mapStateToProps)(PhoneNumber);
