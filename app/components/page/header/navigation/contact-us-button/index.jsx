import React from 'react';

const ContactUsButton = () => (
  <button className="btn btn-primary">Contact Us</button>
);

export default ContactUsButton;
