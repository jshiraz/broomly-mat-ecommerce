import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Logo = () => {
  const imageUri = '/dist/images/logo-maidsaroundtown.png';
  const imageTitle = 'Maids Around Town Logo';
  const imageAlt = 'Maids Around Town Logo';

  return (
    <div className="Logo">
      <Link to="/"><img src={imageUri} title={imageTitle} alt={imageAlt} /></Link>
    </div>
  );
};

Logo.propTypes = {
  imageUri: PropTypes.string,
  imageTitle: PropTypes.string,
  imageAlt: PropTypes.string,
};

export default Logo;
