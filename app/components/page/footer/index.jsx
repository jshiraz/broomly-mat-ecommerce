import React from 'react';
import { NavLink } from 'react-router-dom';

import PhoneNumber from 'components/phone-number';

const Footer = () => (
  <footer className="Footer">
    <div className="FooterNav__container">
      <nav className="FooterNav">
        <ul className="FooterNav__col">
          <li className="FooterNav__title">Company</li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/about-us">About Us</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/become-a-cleaner">Become a Cleaner</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="https://maidsaroundtown.com/maid-service-blog">Around the Home Blog</NavLink></li>
        </ul>
        <ul className="FooterNav__col">
          <li className="FooterNav__title">Cities</li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/austin-tx">Austin</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/dallas-tx">Dallas</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/houston-tx">Houston</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/san-antonio-tx">San Antonio</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/san-diego-tx">San Diego</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="/city/sitemap">Site Map</NavLink></li>
        </ul>
        <ul className="FooterNav__col">
          <li className="FooterNav__title">Services</li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="#">Home Cleaning</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="#">Apartment Cleaning</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="#">Move In/Out Cleaning</NavLink></li>
          <li className="FooterNav__item"><NavLink className="FooterNav__link" to="#">Rental/STR Cleaning</NavLink></li>
        </ul>
        <ul className="FooterNav__col">
          <li className="FooterNav__title">Contact Us</li>
          <li className="FooterNav__item">Email Us</li>
          <li className="FooterNav__item"><a href="mailto:getclean@maidsaroundtown.com">getclean@maidsaroundtown.com</a></li>
          <li className="FooterNav__item">Or by Calling</li>
          <li className="FooterNav__item"><PhoneNumber /></li>
        </ul>
      </nav>
      <div className="Footer__Copyright">
        <p>Copyright &copy; { new Date().getFullYear() } Maids Around Town</p>
      </div>
      <div className="Footer__PolicyNav">
        <ul className="FooterNav__col">
          <li className="FooterNav__item">
            <NavLink className="FooterNav__link" to="/terms-of-service">Terms of Service</NavLink>
          </li>
          <li className="FooterNav__item">
            <NavLink className="FooterNav__link" to="/privacy-policy">Privacy Policy</NavLink>
          </li>
          <li className="FooterNav__item">
            <NavLink className="FooterNav__link" to="/cancellation-policy">Cancellation Policy</NavLink>
          </li>
        </ul>
      </div>
    </div>
  </footer>
);

export default Footer;
