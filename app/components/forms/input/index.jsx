import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import noop from 'lodash/noop';

const Input = ({
  className,
  isError,
  isValid,
  placeholder,
  htmlId,
  value,
  ...rest
}) => {
  const classes = classNames(className, {
    'BookingForm-input': true,
    'is-invalid': isError,
    'is-valid': isValid,
  });
  return <input {...rest} className={classes} type="text" name={htmlId} id={htmlId} placeholder={placeholder} value={value} />;
};

/**
 * Prop types
 */
Input.propTypes = {
  className: PropTypes.string,
  htmlId: PropTypes.string,
  isError: PropTypes.bool,
  isValid: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

/**
 * Default props
 */
Input.defaultProps = {
  className: '',
  htmlId: '',
  isError: false,
  isValid: false,
  placeholder: '',
  value: '',
};

export default Input;
