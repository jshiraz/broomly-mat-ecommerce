/**
 * External dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

/**
 * Counter component for the booking form.
 * @param {Number} cols - Number of columns the input will span.
 * @param {Function} handleDecrement - Function to handle lowering the value of the counter.
 * @param {Function} handleInccrement - Function to handle increasing the value of the counter.
 * @param {String} htmlId - Id provided by the database to name the counter.
 * @param {Number} max - Maximum value of the counter.
 * @param {Number} min - Minimum value of the counter.
 * @param {String} plural - The plural name of the value you are counting.
 * @param {String} singular - The singular name of the value you are counting.
 * @param {Number} value - The value in the form state.
 */
const Counter = ({
  cols,
  handleDecrement,
  handleIncrement,
  htmlId,
  name,
  options,
  value,
}) => (
  <div className="Counter">
    <button onClick={event => handleDecrement(options.min, event)} data-target={htmlId} className="Counter__button Counter__button-dec">-</button>
    <span className="Counter__label">{value} {value > 1 ? name.plural : name.singular}</span>
    <input type="text" id={htmlId} name={htmlId} value={value} hidden readOnly />
    <button onClick={event => handleIncrement(options.max, event)} data-target={htmlId} className="Counter__button Counter__button-inc">+</button>
  </div>
);

/**
 * Prop types
 */
Counter.propTypes = {
  cols: PropTypes.number,
  handleDecrement: PropTypes.func,
  handleIncrement: PropTypes.func,
  htmlId: PropTypes.string,
  name: PropTypes.object,
  options: PropTypes.object,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

/**
 * Default props
 */
Counter.defaultProps = {
  cols: 12,
  handleDecrement: noop,
  handleIncrement: noop,
  htmlId: '',
  options: {},
  name: {
    plural: '',
    singluar: '',
  },
  value: 0,
};

export default Counter;
