import React from 'react';
import classNames from 'classnames';

const Label = ({ ...props, className, children, width }) => {
  const classes = classNames('BookingForm-label', className, {
    'col-12': width === 'full',
    'col-6': width === 'half',
  });

  return (
    <label {...props} className={classes}>
      {children}
    </label>
  );
};

export default Label;
