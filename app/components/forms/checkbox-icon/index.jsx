import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import noop from 'lodash/noop';

/**
 * Checkbox Icons used on the first page of the booking form.
 * @param {String} htmlId - id associated with the input for use in redux state.
 * @param {String} text - label text to describe the checkbox.
 * @param {String} options - extra options provided to the checkbox.
 * @param {Boolean} isChecked - used to show if the checkbox has been checked or not.
 * @param {Function} handleChange - handles changing the value of the input in the redux state.
 */
const CheckboxIcon = ({
  htmlId,
  labelText,
  options,
  isChecked,
  handleToggle,
}) => {
  const classes = classNames('CheckboxIcon__label', {
    'CheckboxIcon__label-checked': isChecked,
  });
  const checkedImageUriSplit = options.imageUri.split('.');
  const checkedImageUri = checkedImageUriSplit[0].concat('-green.', checkedImageUriSplit[1]);

  return (
    <div className="CheckboxIcon">
      <label htmlFor={htmlId} className={classes}>
        <span className="CheckboxIcon__title">{labelText}</span>
        <img className="CheckboxIcon__image" src={isChecked ? checkedImageUri : options.imageUri} alt={labelText} />
        <input type="checkbox" id={htmlId} name={htmlId} checked={isChecked} onChange={handleToggle} hidden />
      </label>
    </div>
  );
};

/**
 * Prop types
 */
CheckboxIcon.propTypes = {
  htmlId: PropTypes.string,
  labelText: PropTypes.string,
  options: PropTypes.shape({
    imageUri: PropTypes.string,
  }),
  isChecked: PropTypes.bool,
  handleToggle: PropTypes.func,
};

/**
 * Default props
 */
CheckboxIcon.defaultProps = {
  htmlId: '',
  labelText: '',
  options: {
    imageUri: '',
  },
  isChecked: false,
  handleToggle: noop,
};

export default CheckboxIcon;
