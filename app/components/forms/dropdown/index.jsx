import React from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

/**
 * Counter component for the booking form.
 * @param {Number} cols - Number of columns the input will span.
 * @param {Function} handleDecrement - Function to handle changing the value of
 * the dropdown in the redux state.
 * @param {String} htmlId - Id provided by the database to name the counter.
 * @param {String} labelText - Text that will be displaed to the left of the dropdown box.
 * @param {Object} options - contains selectOptions as an array that is used to
 * populate the select values.
 * @param {Number} value - The value in the form state.
 */
const Dropdown = ({
  cols,
  handleChange,
  htmlId,
  labelText,
  options,
  value,
}) => (
  <div className="Dropdown">
    <label htmlFor={htmlId} className="Dropdown__label">
      <span className="Dropdown__text">{labelText}</span>
      <select className="Dropdown__select" name={htmlId} id={htmlId} value={value} onChange={handleChange}>
        {
          options.selectValues.map(selectOption => (
            <option key={selectOption.value} value={selectOption.value}>{selectOption.text}</option>
          ))
        }
      </select>
    </label>
  </div>
);

/**
 * Prop types
 */
Dropdown.propTypes = {
  cols: PropTypes.number,
  handleChange: PropTypes.func,
  htmlId: PropTypes.string,
  labelText: PropTypes.string,
  options: PropTypes.object,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

/**
 * Default props
 */
Dropdown.defaultProps = {
  cols: 6,
  handleChange: noop,
  htmlId: '',
  labelText: '',
  options: {},
  value: '',
};

export default Dropdown;
