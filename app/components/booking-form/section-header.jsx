import React from 'react';

const SectionHeader = ({ className, children, ...props}) => (
  <h2 className="BookingForm-section_title">{children}</h2>
);

export default SectionHeader;
