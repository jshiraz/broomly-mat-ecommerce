import React from 'react';

const Section = ({ children, className, props }) => (
  <section className="BookingForm-section" {...props}>
    {children}
  </section>
);
 
export default Section;
