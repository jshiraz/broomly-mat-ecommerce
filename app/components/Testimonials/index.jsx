import React from 'react';

const Testimonials = ({ title, testimonials }) => (
  <div className="Testimonials">
    <section className="Testimonials-content">
      <h2 className="Testimonials-title">{title}</h2>
      <img src="/dist/images/testimonial_star_rating.png" alt="Four point eight star rating." title="4.8 Stars" />
      <p className="Testimonials-text">I have never been able to afford to have my house cleaned before, but I saw how inexpensive these guys are and decided to give them a try. I'm so glad I did! The service was incredible---they are very friendly and do an awesome job. It's such a blessing to have a clean, sparkling house. I recommend them to everyone.</p>
      <p className="Testimonials-person">- B. Johnsen</p>
    </section>
  </div>
);

export default Testimonials;
