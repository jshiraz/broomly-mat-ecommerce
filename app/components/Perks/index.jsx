import React from 'react';

const PerksVertical = props => (
  <div className="Perks__vertical">
    <div className="Perks__vertical-box">
      <img src="/dist/images/guaranteed_service.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
      <section className="Perks__vertical-content_box">
        <h2>Guaranteed Service</h2>
        <p>Satifaction guaranteed or we re-clean at no extra cost.</p>
      </section>
    </div>
    <div className="Perks__vertical-box">
      <img src="/dist/images/eco_friendly.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
      <section className="Perks__vertical-content_box">
        <h2>Eco Friendly</h2>
        <p>We use all top-shelf eco-friendly supplies.</p>
      </section>
    </div>
    <div className="Perks__vertical-box">
      <img src="/dist/images/top_cleaners.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
      <section className="Perks__vertical-content_box">
        <h2>Top Cleaners</h2>
        <p>Our cleaners are thoroughly trained to provide you with a 5 star experience.</p>
      </section>
    </div>
    <div className="Perks__vertical-box">
      <img src="/dist/images/online_text_management.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
      <section className="Perks__vertical-content_box">
        <h2>Online & Text Management</h2>
        <p>Simply text us for any questions or modifications regarding your appointment.</p>
      </section>
    </div>
  </div>
);

export default PerksVertical;
