/**
 * External dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import v4 from 'uuid/v4';
import noop from 'lodash/noop';

/**
 * Internal dependencies
 */
import CheckboxIcon from 'components/forms/checkbox-icon';
import Counter from 'components/forms/counter';
import Dropdown from 'components/forms/dropdown';
import formSupport from 'services/form-support';
import Loading from 'components/loading';
import Section from 'components/booking-form/section';
import SectionHeader from 'components/booking-form/section-header';
import StepController from '../step-controller';
import {
  handleCheckboxInputChange,
  handleCounterDecrement,
  handleCounterIncrement,
  handleTextInputChange,
  setCleaningHours,
  setCurrentStep,
  setStepTitle,
} from 'state/booking-form/actions';

class CleaningInfoForm extends React.Component {
  constructor() {
    super();
    this.state = {
      sections: [],
    };
    this.nextStep = this.nextStep.bind(this);
  }

  componentDidMount() {
    const {
      currentForm,
      currentStep,
      setTitle,
    } = this.props;

    formSupport.fetchForm(currentForm, currentStep)
      .then((form) => {
        this.setState({
          sections: form.sections,
        });
        setTitle(form.title);
      })
      .catch(() => { });
  }

  componentDidUpdate(prevProps) {
    const { currentForm, currentStep, setTitle } = this.props;
    const prevForm = prevProps.currentForm;

    if (prevForm !== currentForm) {
      formSupport.fetchForm(currentForm, currentStep)
        .then((form) => {
          this.setState({
            sections: form.sections,
          });
          setTitle(form.title);
        })
        .catch(() => { });
    }
  }

  nextStep() {
    const { form, setHours } = this.props;
    setHours(formSupport.calculateCleaningHours(form));
    this.props.setStep();
  }

  /**
   * Renders a field to the page.
   * @param {Object} section - section of the form to be rendered as an object.
   */
  renderField(section) {
    const {
      handleChange,
      handleDecrement,
      handleIncrement,
      handleToggle,
    } = this.props;
    return section.fields.map((field) => {
      switch (field.type) {
        case 'counter':
          return (
            <Counter
              key={v4()}
              value={this.props.form[field.htmlId]}
              {...field}
              handleDecrement={handleDecrement}
              handleIncrement={handleIncrement}
            />
          );
        case 'dropdown':
          return (
            <Dropdown
              key={v4()}
              value={this.props.form[field.htmlId]}
              {...field}
              handleChange={handleChange}
            />
          );
        case 'checkboxIcon':
          return (
            <CheckboxIcon
              key={v4()}
              isChecked={this.props.form[field.htmlId]}
              handleToggle={handleToggle}
              {...field}
            />
          );
        default:
          return null;
      }
    });
  }

  renderSections() {
    return this.state.sections.map((section) => {
      return (
        <Section key={v4()}>
          {section.title ? <SectionHeader>{section.title}</SectionHeader> : null}
          {this.renderField(section)}
        </Section>
      );
    });
  }

  render() {
    const { children, setStep } = this.props;
    if (this.state.sections.length === 0) {
      return (
        <div className="BookingForm">
          {children}
          <div className="BookingForm-form_bg">
            <Section>
              <Loading />
            </Section>
          </div>
        </div>
      );
    }
    return (
      <div className="BookingForm">
        {children}
        <div className="BookingForm-form_bg">
          {this.renderSections()}
          <Section>
            <StepController text="Get an Instant Online Pricing Estimate" handleClick={this.nextStep} />
          </Section>
        </div>
      </div>
    );
  }
}

/**
 * Prop types
 */
CleaningInfoForm.propTypes = {
  handleChange: PropTypes.func,
  handleDecrement: PropTypes.func,
  handleIncrement: PropTypes.func,
  handleToggle: PropTypes.func,
  currentStep: PropTypes.number,
  currentForm: PropTypes.string,
  form: PropTypes.object,
  setHours: PropTypes.func,
  setStep: PropTypes.func,
  setTitle: PropTypes.func,
  status: PropTypes.string,
};

/**
 * Default props
 */
CleaningInfoForm.defaultProps = {
  handleChange: noop,
  handleDecrement: noop,
  handleIncrement: noop,
  handleToggle: noop,
  currentStep: 1,
  currentForm: '',
  form: {},
  setHours: noop,
  setStep: noop,
  setTitle: noop,
  status: 'PENDING',
};

const mapStateToProps = state => ({
  ...state.bookingForm,
});

const mapDispatchToProps = dispatch => ({
  handleToggle: event => dispatch(handleCheckboxInputChange(event)),
  handleChange: event => dispatch(handleTextInputChange(event)),
  handleDecrement: (max, event) => dispatch(handleCounterDecrement(max, 1, event)),
  handleIncrement: (min, event) => dispatch(handleCounterIncrement(min, 1, event)),
  setStep: () => dispatch(setCurrentStep(2)),
  setTitle: title => dispatch(setStepTitle(title)),
  setHours: hours => dispatch(setCleaningHours(hours)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CleaningInfoForm);
