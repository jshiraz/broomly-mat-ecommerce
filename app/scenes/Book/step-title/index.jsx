/**
 * External dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';

const StepTitle = ({ title }) => (
  <section className="Book__StepTitle">
    <h1>{title}</h1>
  </section>
);

/**
 * Prop types
 */
StepTitle.propTypes = {
  title: PropTypes.string,
};

/**
 * Default props
 */
StepTitle.defaultProps = {
  title: '',
};

export default StepTitle;
