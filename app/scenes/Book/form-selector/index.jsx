import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class FormSelector extends Component {
  constructor(props) {
    super();
  }

  setClassName(slug) {
    let className = 'FormSelector__title';
    if (this.props.selected === slug) {
      className = className.concat(' FormSelector__title--active');
    }

    return className;
  }

  render() {
    return (
      <div className="FormSelector">
        {
          this.props.formsArray.map(form => (
            <Link key={form.formTitle} to={`/book/${form.formTitle.toLowerCase()}`} className="FormSelector__col">
              <h2 className={this.setClassName(form.slug)} id={form.formTitle}>{form.formTitle}</h2>
            </Link>
          ))
        }
      </div>
    );
  }
}

export default FormSelector;
