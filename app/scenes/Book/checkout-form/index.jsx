/**
 * External dependencies
 */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { debounce, throttle, noop } from 'lodash';
import { injectStripe, CardNumberElement, CardCVCElement, CardExpiryElement } from 'react-stripe-elements';

/**
 * Internal dependencies
 */
import formSupport from 'services/form-support';
import Section from 'components/booking-form/section';
import SectionHeader from 'components/booking-form/section-header';
import Label from 'components/forms/label';
import StepController from '../step-controller';
import zoho from 'services/zoho';
import {
  handleTextInputChange,
  handleStripeIdInsertion,
} from 'state/booking-form/actions';

class CheckoutForm extends React.Component {
  constructor(props) {
    super();
    this.state = {
      cardErrors: {
        cNum: {
          message: '',
        },
        expDate: {
          message: '',
        },
        cvc: {
          message: '',
        },
      },
    };
    this.validateInput = this.validateInput.bind(this);
    this.automaticZohoPost = this.automaticZohoPost.bind(this);
    this.debounceAutomaticZohoPost = debounce(this.automaticZohoPost, 3000);
    this.throttleConvertToContact = throttle(zoho.convertToContact, 5000);
  }

  /**
   * When the component updates it calls automaticZohoPost() to upsert new data into Zoho CRM.
   */
  componentDidUpdate() {
    this.debounceAutomaticZohoPost();
  }

  setStripeCCErrorMessage(message) {
    this.setState({
      cardErrors: {
        cNum: {
          message,
        },
        expDate: { message: '' },
        cvc: { message: '' },
      },
    });
  }

  setStripeExpErrorMessage(message) {
    this.setState({
      cardErrors: {
        cNum: { message: '' },
        expDate: { message },
        cvc: { message: '' },
      },
    });
  }

  setStripeCVCErrorMessage(message) {
    this.setState({
      cardErrors: {
        cNum: { message: '' },
        expDate: { message: '' },
        cvc: { message },
      },
    });
  }

  clearStripeErrorMessage() {
    this.setState({
      cardErrors: {
        cNum: { message: '' },
        expDate: { message: '' },
        cvc: { message: '' },
      },
    });
  }

  /**
   * Handles the errors that are passed by Stripe when you try to create a token from CC info.
   * @param {object} stripeError - the object that gets returned by
   * Stripe when you try to create a token.
   */
  stripeErrorHandler(stripeError) {
    switch (stripeError.code) {
      case 'incomplete_number':
        this.setStripeCCErrorMessage(stripeError.message);
        break;
      case 'invalid_number':
        this.setStripeCCErrorMessage(stripeError.message);
        break;
      case 'incomplete_expiry':
        this.setStripeExpErrorMessage(stripeError.message);
        break;
      case 'invalid_expiry_year':
        this.setStripeExpErrorMessage(stripeError.message);
        break;
      case 'invalid_expiry_year_past':
        this.setStripeExpErrorMessage(stripeError.message);
        break;
      case 'incomplete_cvc':
        this.setStripeCVCErrorMessage(stripeError.message);
        break;
      default:
        this.clearStripeErrorMessage();
        break;
    }
  }

  /**
   * Validates the form input for the CheckoutForm component.
   */
  validateInput() {
    const { form, handleStripeId } = this.props;
    const formInputArray = [
      'firstName',
      'lastName',
      'address',
      'city',
      'state',
      'zipCode',
    ];
    const errorArray = [];
    let cardObj = {
      type: 'card',
      name: form.firstName + ' ' + form.lastName,
      address_city: form.city,
      address_line1: form.address,
      address_state: form.state,
      address_zip: form.zipCode,
    };

    if (form.aptSuite !== '') {
      cardObj = Object.assign(cardObj, { address_line2: form.aptSuite });
    }

    formInputArray.forEach((inputName) => {
      document.getElementById(inputName).classList.remove('is-invalid');
      if (form[inputName] === '' && errorArray.indexOf(inputName) === -1) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
    });

    this.props.stripe.createToken(cardObj)
      .then((token) => {
        if (typeof token.token === 'undefined') {
          this.stripeErrorHandler(token.error);
          return false;
        }
        this.clearStripeErrorMessage();
        if (errorArray.length === 0) {
          handleStripeId(token.token);
          this.throttleConvertToContact(form);
        }
        return true;
      });
  }

  /**
   * Once the required fields in the formInputArray have been filled out the app will start
   * automatically upserting the data into Zoho CRM
   */
  automaticZohoPost() {
    const { form } = this.props;
    const formInputArray = [
      'firstName',
      'lastName',
      'address',
      'city',
      'state',
      'zipCode',
    ];
    const isComplete = formInputArray.reduce((accu, stateVariable) => {
      if (form[stateVariable] === '') {
        return false;
      }
      return accu;
    }, true);

    if (isComplete) {
      zoho.upsertLead(form);
    }
  }

  renderCCError() {
    if (this.state.cardErrors.cNum.message !== '') {
      return this.state.cardErrors.cNum.message;
    }
    return '';
  }

  renderExpError() {
    if (this.state.cardErrors.expDate.message !== '') {
      return this.state.cardErrors.expDate.message;
    }
    return '';
  }

  renderCVCError() {
    if (this.state.cardErrors.cvc.message !== '') {
      return this.state.cardErrors.cvc.message;
    }
    return '';
  }

  /**
   * Renders the component.
   */
  render() {
    const { form, handleChange } = this.props;

    return (
      <form className="CheckoutForm">
        <div className="BookingForm-form_bg">
          <Section>
            <SectionHeader>Service Address</SectionHeader>
            <Label htmlFor="firstName" width="half">
              <span className="BookingForm-label_text">First Name</span>
              <input className="BookingForm-input" type="text" name="firstName" id="firstName" placeholder="First Name" onChange={handleChange} value={form.firstName} />
            </Label>
            <Label htmlFor="lastName" width="half">
              <span className="BookingForm-label_text">Last Name</span>
              <input className="BookingForm-input" type="text" name="lastName" id="lastName" placeholder="Last Name" onChange={handleChange} value={form.lastName} />
            </Label>
            <Label className="col-9" htmlFor="address">
              <span className="BookingForm-label_text">Address</span>
              <input className="BookingForm-input" type="text" name="address" id="address" placeholder="Address" onChange={handleChange} value={form.address} />
            </Label>
            <Label className="col-3" htmlFor="aptSuite">
              <span className="BookingForm-label_text">Apt/Suite</span>
              <input className="BookingForm-input" type="text" name="aptSuite" id="aptSuite" placeholder="Apt/Suite" onChange={handleChange} value={form.aptSuite} />
            </Label>
            <Label className="col-5" htmlFor="city">
              <span className="BookingForm-label_text">City</span>
              <input className="BookingForm-input" type="text" name="city" id="city" placeholder="City" onChange={handleChange} value={form.city} />
            </Label>
            <Label className="col-3" htmlFor="state">
              <span className="BookingForm-label_text">State</span>
              <input className="BookingForm-input" type="text" name="state" id="state" placeholder="State" onChange={handleChange} value={form.state} />
            </Label>
            <Label className="col-4" htmlFor="zipCode">
              <span className="BookingForm-label_text">Zip Code</span>
              <input className="BookingForm-input" type="text" name="zipCode" id="zipCode" placeholder="Zip Code" onChange={handleChange} value={form.zipCode} />
            </Label>
          </Section>
        </div>
        <div className="BookingForm-form_bg">
          <Section>
            <SectionHeader>Payment</SectionHeader>
            <p className="BookingForm-section_desc col-12">Your credit card won&apos;t be charged until after your appointment.</p>
            <Label htmlFor="creditCard" width="full">
              <span className="BookingForm-label_text">Credit Card</span>
              <CardNumberElement className="form-control" id="creditCard" style={{ base: { fontSize: '16px' } }} />
              <span className="ccError">{this.renderCCError()}</span>
            </Label>
            <Label className="col-sm-3" htmlFor="expiration" width="half">
              <span className="BookingForm-Label_text">Exp Date</span>
              <CardExpiryElement className="form-control" id="expiration" style={{ base: { fontSize: '16px' } }} />
              <span className="ccError">{this.renderExpError()}</span>
            </Label>
            <Label className="col-sm-3" htmlFor="cvc" width="half">
              <span className="BookingForm-Label_text">CVC</span>
              <CardCVCElement className="form-control" id="cvc" style={{ base: { fontSize: '16px' } }} />
              <span className="ccError">{this.renderCVCError()}</span>
            </Label>
            <div className="col-6 col-sm-3 d-flex align-items-center">
              <img src="/dist/images/powered_by_stripe.svg" alt="Powered by Stripe" title="Powered by Strip icon" />
            </div>
            <div className="col-6 col-sm-3 d-flex align-items-center CheckoutForm-cc_icons">
              <img src="/dist/images/visa.png" alt="VISA" title="VISA icon" />
              <img src="/dist/images/mastercard.png" alt="Mastercard" title="Mastercard icon" />
              <img src="/dist/images/discover.png" alt="Discover" title="Discover icon" />
              <img src="/dist/images/amex.png" alt="American Express" title="American Express icon" />
            </div>
          </Section>
          <Section>
            <StepController text="Place Order" handleClick={this.validateInput} />
          </Section>
        </div>
      </form>
    );
  }
}

/**
 * Prop types
 */
CheckoutForm.propTypes = {
  handleChange: PropTypes.func,
  handleStripeId: PropTypes.func,
  currentStep: PropTypes.number,
  currentForm: PropTypes.string,
  form: PropTypes.object,
  setStep: PropTypes.func,
  setTitle: PropTypes.func,
};

/**
 * Default props
 */
CheckoutForm.defaultProps = {
  handleChange: noop,
  handleStripId: noop,
  currentStep: 4,
  currentForm: '',
  form: {},
  setStep: noop,
  setTitle: noop,
};

const mapStateToProps = state => ({
  ...state.bookingForm,
});

const mapDispatchToProps = dispatch => ({
  handleChange: event => dispatch(handleTextInputChange(event)),
  handleStripeId: stripeId => dispatch(handleStripeIdInsertion(stripeId)),
  setStep: () => dispatch(setCurrentStep(5)),
  setTitle: title => (dispatch(setStepTitle(title))),
});

export default injectStripe(connect(mapStateToProps, mapDispatchToProps)(CheckoutForm));
