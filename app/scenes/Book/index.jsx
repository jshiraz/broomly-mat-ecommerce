/**
 * External dependencies
 */
import React from 'react';
import Loadable from 'react-loadable';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Elements } from 'react-stripe-elements';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

/**
 * Internal dependencies
 */
import Body from 'components/page/body';
import CheckoutWidget from './checkout-widget';
import dummyData from 'services/dummydata'; // Remove this once db has been hooked up
import FormSelector from './form-selector';
import Loading from 'components/loading';
import StepTitle from './step-title';
import Perks from 'components/perks';
import ProgressBar from './progress-bar';
import Testimonials from 'components/testimonials';
import { fetchInitialBookingFormState, setCurrentStep } from 'state/booking-form/actions';

/**
 * Loadable Components
 */
const CleaningInfoForm = Loadable({
  loader: () => import('./cleaning-info-form'),
  loading: Loading,
});
const CheckoutForm = Loadable({
  loader: () => import('./checkout-form'),
  loading: Loading,
});
const ContactForm = Loadable({
  loader: () => import('./contact-form'),
  loading: Loading,
});
const ScheduleForm = Loadable({
  loader: () => import('./schedule-form'),
  loading: Loading,
});

class Book extends React.Component {
  componentDidMount() {
    this.props.fetchInitialState(this.props.match.params.slug);
  }

  componentDidUpdate() {
    if (this.props.currentForm !== this.props.match.params.slug) {
      this.props.fetchInitialState(this.props.match.params.slug);
    }
  }

  componentWillUnmount() {
    this.props.setStep(1);
  }

  /**
   * Renders the current form based on what step it is.
   */
  renderCurrentForm() {
    const { currentStep } = this.props;

    switch (currentStep) {
      case 1:
        return (
          <CleaningInfoForm>
            <FormSelector
              formsArray={dummyData.bookingInfo}
              selected={this.props.match.params.slug}
            />
          </CleaningInfoForm>
        );
      case 2:
        return <ContactForm />;
      case 3:
        return <ScheduleForm />;
      case 4:
        return (
          <Elements>
            <CheckoutForm />
          </Elements>
        );
      default: return null;
    }
  }

  /**
   * Renders the Checkout Widget in the sidebar if the current step 4
   */
  renderCheckoutWidget() {
    const { currentStep } = this.props;
    if (currentStep === 4) {
      return (<CheckoutWidget />);
    }
    return null;
  }

  /**
   * Renders the Testimonial component if the current step is not 4
   */
  renderTestimonials() {
    const { currentStep } = this.props;
    if (currentStep !== 4) {
      return (<Testimonials title="Ratings" />);
    }
    return null;
  }

  render() {
    const { currentStep, stepTitle } = this.props;

    return (
      <Body title="Maids Around Town - Book Your Cleaning" bodyClass="Book">
        <Helmet>
          <title>Maids Around Town Austin - Book a Cleaning</title>
          <meta name="keywords" content="maids around town, book a cleaning, maid service austin" />
        </Helmet>
        <StepTitle title={stepTitle} />
        <ProgressBar currentStep={currentStep} />
        <div className="Book__content">
          <div className="Book__container">
            <div className="Book__row">
              {this.renderCurrentForm()}
              <div className="Book__Sidebar">
                {this.renderCheckoutWidget()}
                <Perks direction="row" />
                {this.renderTestimonials()}
              </div>
            </div>
          </div>
        </div>
      </Body>
    );
  }
}

/**
 * Prop types
 */
Book.propTypes = {
  fetchInitialState: PropTypes.func,
  currentStep: PropTypes.number,
  currentForm: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
  stepTitle: PropTypes.string,
};

/**
 * Default props
 */
Book.defaultProps = {
  currentStep: 1,
  currentForm: '',
  fetchInitialState: noop,
  stepTitle: 'Get a quote and book in 15 seconds!',
};

const mapStateToProps = state => ({
  currentStep: state.bookingForm.currentStep,
  stepTitle: state.bookingForm.stepTitle,
  currentForm: state.bookingForm.currentForm,
});

const mapDispatchToProps = dispatch => ({
  fetchInitialState: slug => dispatch(fetchInitialBookingFormState(slug)),
  setStep: step => dispatch(setCurrentStep(step)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Book));
