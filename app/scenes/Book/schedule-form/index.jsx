/**
 * External dependencies
 */

import React from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import { debounce, noop } from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';

/**
 * Internal dependencies
 */
import Counter from 'components/forms/counter';
import CouponCode from './coupon-code';
import SavingsDisplay from './savings-display';
import Label from 'components/forms/label';
import Section from 'components/booking-form/section';
import SectionHeader from 'components/booking-form/section-header';
import StepController from '../step-controller';
import FreqSelector from './freq-selector';
import zoho from 'services/zoho';
import {
  handleCheckboxInputChange,
  handleCounterDecrement,
  handleCounterIncrement,
  handleDateChange,
  handleTextInputChange,
  handleRadioInputChange,
  setStepTitle,
  setCurrentStep,
} from 'state/booking-form/actions';

class ScheduleForm extends React.Component {
  constructor() {
    super();
    this.validateInput = this.validateInput.bind(this);
    this.automaticZohoPost = this.automaticZohoPost.bind(this);
    this.debounceAutomaticZohoPost = debounce(this.automaticZohoPost, 3000);
  }

  componentDidUpdate() {
    this.debounceAutomaticZohoPost();
  }

  validateInput() {
    const { form, setStep } = this.props;
    const formInputArray = [
      'date',
      'arrivalTime',
    ];
    const errorArray = [];

    formInputArray.forEach((inputName) => {
      document.getElementById(inputName).classList.remove('is-invalid');
      if (form.date === null && errorArray.indexOf(inputName) === -1) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
      if (form[inputName] === '' && errorArray.indexOf(inputName) === -1) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
    });

    if (errorArray.length === 0) {
      this.debounceAutomaticZohoPost();
      setStep();
    }
  }

  automaticZohoPost() {
    const { form } = this.props;
    const formInputArray = [
      'date',
      'arrivalTime',
      'frequency',
    ];
    const isComplete = formInputArray.reduce((accu, stateVariable) => {
      if (form[stateVariable] === '') {
        return false;
      }
      return accu;
    }, true);

    if (isComplete) {
      zoho.upsertLead(form);
    }
  }

  render() {
    const {
      form,
      handleChange,
      handleDecrement,
      handleDate,
      handleIncrement,
      handleRadio,
      handleToggle,
    } = this.props;

    return (
      <div className="ScheduleForm">
        <div className="BookingForm-form_bg">
          <Section>
            <SectionHeader>When can we help you?</SectionHeader>
            <Label className="BookingForm-datepicker" htmlFor="datepicker" width="half" >
              <span className="BookingForm-label_text">Preferred Date</span>
              <DatePicker
                onChange={date => handleDate(date)}
                className="BookingForm-input form-control"
                selected={form.date}
                name="date"
                id="date"
                minDate={moment().add(3, 'days')}
              />
            </Label>
            <Label htmlFor="arrivalTime" width="half">
              <span className="BookingForm-label_text">Preferred Arrival Time</span>
              <select className="BookingForm-select" type="text" name="arrivalTime" id="arrivalTime" value={form.arrivalTime} onChange={handleChange}>
                <option value="8:00AM">8:00AM</option>
                <option value="8:30AM">8:30AM</option>
                <option value="9:00AM">9:00AM</option>
                <option value="9:30AM">9:30AM</option>
                <option value="10:00AM">10:00AM</option>
                <option value="10:30AM">10:30AM</option>
                <option value="11:00AM">11:00AM</option>
                <option value="11:30AM">11:30AM</option>
                <option value="12:00PM">12:00PM</option>
                <option value="12:30PM">12:30PM</option>
                <option value="1:00PM">1:00PM</option>
                <option value="1:30PM">1:30PM</option>
                <option value="2:00PM">2:00PM</option>
                <option value="2:30PM">2:30PM</option>
                <option value="3:00PM">3:00PM</option>
                <option value="3:30PM">3:30PM</option>
                <option value="4:00PM">4:00PM</option>
              </select>
            </Label>
            <p className="col-12">We will check availability and confirm a date and time</p>
            <SectionHeader>How often should we come?</SectionHeader>
            <div className="BookingForm-FreqSelector">
              <FreqSelector form={form} handleRadioChange={handleRadio} />
            </div>
            <Label htmlFor="flex" width="full">
              <input type="checkbox" name="flex" id="flex" value="flex" onClick={handleToggle} />
              <span className="BookingForm-check_text"><strong>Save with Flex! (8am - 4pm)</strong></span>
            </Label>
            <p className="col-12"><strong className="text-primary">Save an additional %6.25 with Flex!</strong> Let us show up anytime on your selected date (arrive between 8am - 4pm) and we&apos;ll give you this extra discount.</p>
          </Section>
        </div>
        <div className="BookingForm-form_bg">
          <Section>
            <h2 className="BookingForm-section_title">Price &amp; Savings</h2>
            <p className="BookingForm-section_desc col-12">Recommended Time</p>
            <Counter
              handleDecrement={handleDecrement}
              handleIncrement={handleIncrement}
              value={form.cleaningHours}
              htmlId="cleaningHours"
              name={{
                plural: 'Cleaning Hours',
                singular: 'Cleaning Hours',
              }}
              options={{
                min: 2,
                max: 36,
              }}
            />
            <div className="BookingForm-SavingsDisplay col-12">
              <SavingsDisplay form={form} />
            </div>
            <div className="BookingForm-CouponCode col-12">
              <CouponCode form={form} handleInputChange={handleChange} handleCouponCode={() => noop} />
            </div>
          </Section>
          <Section>
            <StepController text="Get Quote and Review Order" handleClick={this.validateInput} />
          </Section>
        </div>
      </div>
    );
  }
}

/**
 * Prop types
 */
ScheduleForm.propTypes = {
  handleChange: PropTypes.func,
  handleDate: PropTypes.func,
  handleDecrement: PropTypes.func,
  handleIncrement: PropTypes.func,
  handleToggle: PropTypes.func,
  handleRadio: PropTypes.func,
  currentStep: PropTypes.number,
  currentForm: PropTypes.string,
  form: PropTypes.object,
  setStep: PropTypes.func,
  setTitle: PropTypes.func,
  status: PropTypes.string,
};

/**
 * Default props
 */
ScheduleForm.defaultProps = {
  handleChange: noop,
  handleDate: noop,
  handleToggle: noop,
  handleRadio: noop,
  currentStep: 2,
  currentForm: '',
  form: {},
  setStep: noop,
  setTitle: noop,
  status: 'PENDING',
};

const mapStateToProps = state => ({
  ...state.bookingForm,
});

const mapDispatchToProps = dispatch => ({
  handleToggle: event => dispatch(handleCheckboxInputChange(event)),
  handleChange: event => dispatch(handleTextInputChange(event)),
  handleDecrement: (max, event) => dispatch(handleCounterDecrement(max, 0.5, event)),
  handleIncrement: (min, event) => dispatch(handleCounterIncrement(min, 0.5, event)),
  handleDate: date => dispatch(handleDateChange(date)),
  handleRadio: event => dispatch(handleRadioInputChange(event)),
  setStep: () => dispatch(setCurrentStep(4)),
  setTitle: title => (dispatch(setStepTitle(title))),
});

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleForm);
