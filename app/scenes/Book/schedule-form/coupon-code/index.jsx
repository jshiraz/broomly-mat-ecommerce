import React from 'react';
import classNames from 'classnames';

class CouponCode extends React.Component {
  constructor() {
    super();

    this.state = {
      isValid: false,
      hasChecked: false,
    };

    this.isCouponValid = this.isCouponValid.bind(this);
  }

  isCouponValid() {
    const { form, handleCouponCode } = this.props;

    this.setState({ hasChecked: true });

    if (form.couponCode === 'testCoupon') {
      this.setState({ isValid: true });
      handleCouponCode(0.2);
      return true;
    }
    this.setState({ isValid: false });
    return false;
  }

  render() {
    const { form, handleInputChange } = this.props;
    const classes = classNames('form-control', {
      'is-valid': this.state.isValid,
      'is-invalid': !this.state.isValid && this.state.hasChecked,
    });

    return (
      <div className="CouponCode">
        <input type="text" className={classes} name="couponCode" id="couponCode" placeholder="Optional Coupon" onChange={handleInputChange} value={form.couponCode} />
        <div className="input-group-append">
          <button className="input-group-button btn btn-primary" onClick={this.isCouponValid}>Apply</button>
        </div>
      </div>
    );
  }
}

export default CouponCode;
