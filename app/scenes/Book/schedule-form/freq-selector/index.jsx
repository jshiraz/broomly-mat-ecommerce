import React, { Component } from 'react';

import FreqSelection from './freq-selection';

import dummyData from 'services/dummydata';

class FreqSelector extends Component {
  static calcSavings(frequency) {
    const priceRates = dummyData.priceRates;

    const basePrice = priceRates.normal.once;
    const savingsPrice = priceRates.normal[frequency];

    return ((basePrice - savingsPrice) / basePrice) * 100;
  }

  constructor() {
    super();

    this.isRadioSelected = this.isRadioSelected.bind(this);
  }

  isRadioSelected(freq) {
    const { form } = this.props;

    if (freq === form.frequency) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <div className="FreqSelector">
        <FreqSelection
          htmlId="once"
          text='Once'
          radioGroup="frequency"
          isChecked={this.isRadioSelected('once')}
          handleRadioChange={this.props.handleRadioChange}
        />
        <FreqSelection
          htmlId="monthly"
          text='Monthly'
          savings={FreqSelector.calcSavings('monthly')}
          radioGroup="frequency"
          isChecked={this.isRadioSelected('monthly')}
          handleRadioChange={this.props.handleRadioChange}
        />
        <FreqSelection
          htmlId="biWeekly"
          text='Bi-Weekly'
          savings={FreqSelector.calcSavings('biWeekly')}
          radioGroup="frequency"
          isChecked={this.isRadioSelected('biWeekly')}
          handleRadioChange={this.props.handleRadioChange}
        />
        <FreqSelection
          htmlId="weekly"
          text='Weekly'
          savings={FreqSelector.calcSavings('weekly')}
          radioGroup="frequency"
          isChecked={this.isRadioSelected('weekly')}
          handleRadioChange={this.props.handleRadioChange}
        />
      </div>
    );
  }
}

export default FreqSelector;
