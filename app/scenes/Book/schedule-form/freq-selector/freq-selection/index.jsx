import React from 'react';
import classNames from 'classnames';

class FreqSelection extends React.Component {
  renderSavings() {
    if (typeof this.props.savings !== 'undefined') {
      return (
        <span className="FreqSelection__savings">Save {this.props.savings}%</span>
      );
    }

    return null;
  }

  render() {
    const { htmlId, text, savings, radioGroup, handleRadioChange, isChecked } = this.props;
    const classes = classNames('FreqSelection__box', {
      'FreqSelection__box-selected': isChecked,
    });

    return (
      <div className="FreqSelection">
        <label className={classes} htmlFor={htmlId}>
          <span className="FreqSelection__text">{text}</span>
          {this.renderSavings()}
          <input type="radio" name={radioGroup} id={htmlId} value={htmlId} onChange={handleRadioChange} hidden />
        </label>
      </div>
    );
  }
};

export default FreqSelection;
