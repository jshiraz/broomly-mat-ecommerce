import React from 'react';

import dummyData from 'services/dummydata';

class SavingsDisplay extends React.Component {
  calcInitCost() {
    const { form } = this.props;
    const hourlyRate = form.flex ? dummyData.priceRates.flex : dummyData.priceRates.normal;

    return (form.cleaningHours * hourlyRate[form.frequency]).toFixed(2);
  }

  calcInitSavings() {
    const { form } = this.props;
    const oneTimeCleaningRate = dummyData.priceRates.normal.once;

    return ((form.cleaningHours * oneTimeCleaningRate) - this.calcInitCost()).toFixed(2);
  }

  calcCouponSavings() {
    const { form } = this.props;

    const initCost = this.calcInitCost();

    return (initCost * form.couponDiscount).toFixed(2);
  }

  calcSubTotal() {
    return ((this.calcInitCost() - this.calcCouponSavings()) + parseFloat(dummyData.priceRates.supplies)).toFixed(2);
  }

  calcTotalSavings() {
    return (parseFloat(this.calcInitSavings()) + parseFloat(this.calcCouponSavings())).toFixed(2);
  }

  renderSavings() {
    const savings = this.calcCouponSavings();
    if (savings === '0.00') {
      return null;
    }
    return (
      <div>
        <span className="SavingsDisplay__text text-primary">Coupon Savings:</span>
        <span className="SavingsDisplay__numbers text-primary">-${savings}</span>
      </div>
    );
  }

  render() {
    return (
      <div className="SavingsDisplay">
        <span className="SavingsDisplay__text">Hourly Rate x Number of Hours:</span>
        <span className="SavingsDisplay__numbers">${this.calcInitCost()}</span>
        <span className="SavingsDisplay__text">Name Brand Supplies:</span>
        <span className="SavingsDisplay__numbers">${(dummyData.priceRates.supplies).toFixed(2)}</span>
        {this.renderSavings()}
        <span className="SavingsDisplay__text">Subtotal:</span>
        <span className="SavingsDisplay__numbers">${this.calcSubTotal()}</span>
        <span className="SavingsDisplay__text text-primary">Total Savings:</span>
        <span className="SavingsDisplay__numbers text-primary">-${this.calcTotalSavings()}</span>
      </div>
    );
  }
}

export default SavingsDisplay;
