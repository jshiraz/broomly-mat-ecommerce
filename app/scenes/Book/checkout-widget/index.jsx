import React from 'react';
import { connect } from 'react-redux';
import dummyData from 'services/dummydata';

class CheckoutWidget extends React.Component {
  calcInitCost() {
    const { form } = this.props;
    const hourlyRate = form.flex ? dummyData.priceRates.flex : dummyData.priceRates.normal;

    return ((form.cleaningHours * hourlyRate[form.frequency]) + dummyData.priceRates.supplies).toFixed(2);
  }

  calcInitSavings() {
    const { form } = this.props;
    const oneTimeCleaningRate = dummyData.priceRates.normal.once;

    return ((form.cleaningHours * oneTimeCleaningRate) - this.calcInitCost()).toFixed(2);
  }

  calcCouponSavings() {
    const { form } = this.props;

    const initCost = this.calcInitCost();

    return (initCost * form.couponDiscount).toFixed(2);
  }

  calcTotalSavings() {
    return (parseFloat(this.calcInitSavings()) + parseFloat(this.calcCouponSavings())).toFixed(2);
  }

  calcTax() {
    return (this.calcInitCost() * dummyData.priceRates.taxRate).toFixed(2);
  }

  calcTotalCost() {
    return ((parseFloat(this.calcInitCost()) - parseFloat(this.calcCouponSavings())) + parseFloat(this.calcTax())).toFixed(2);
  }

  renderCouponSavings() {
    const savings = this.calcCouponSavings();
    if (savings === '0.00') {
      return null;
    }
    return (
      <div className="CheckoutWidget__row">
        <span className="CheckoutWidget__text text-primary">Coupon Savings:</span>
        <span className="CheckoutWidget__amount text-primary">-${savings}</span>
      </div>
    );
  }

  render() {
    return (
      <div className="CheckoutWidget">
        <h2 className="CheckoutWidget__title">Checkout Price</h2>
        <div className="CheckoutWidget__content">
          <div className="CheckoutWidget__row">
            <span className="CheckoutWidget__text">Cleaning and Supplies:</span>
            <span className="CheckoutWidget__amount">${this.calcInitCost()}</span>
          </div>
          {this.renderCouponSavings()}
          <hr />
          <div className="CheckoutWidget__row">
            <span className="CheckoutWidget__text">Taxes:</span>
            <span className="CheckoutWidget__amount">${this.calcTax()}</span>
          </div>
          <div className="CheckoutWidget__row">
            <span className="CheckoutWidget__text">Total:</span>
            <span className="CheckoutWidget__amount">${this.calcTotalCost()}</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.bookingForm,
});

export default connect(mapStateToProps)(CheckoutWidget);
