import React from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

const StepController = ({ handleClick, text }) => (
  <div className="StepController">
    <button type="button" onClick={handleClick} className="StepController-button btn btn-primary">{text}</button>
  </div>
);

/**
 * Prop types
 */
StepController.propTypes = {
  handleClick: PropTypes.func,
  text: PropTypes.string,
};

/**
 * Default props
 */
StepController.defaultProps = {
  handleClick: noop,
  text: '',
};

export default StepController;
