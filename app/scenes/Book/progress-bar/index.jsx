import React from 'react';

const ProgressBar = props => (
  <div className="ProgressBar">
    <div className={props.currentStep === 1 ? 'ProgressBar-Step current' : 'ProgressBar-Step'}>
      <span>1</span>
    </div>
    <div className={props.currentStep === 2 ? 'ProgressBar-Step current' : 'ProgressBar-Step'}>
      <span>2</span>
    </div>
    <div className={props.currentStep === 3 ? 'ProgressBar-Step current' : 'ProgressBar-Step'}>
      <span>3</span>
    </div>
  </div>
);

export default ProgressBar;
