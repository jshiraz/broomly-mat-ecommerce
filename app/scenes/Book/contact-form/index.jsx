/**
 * External dependencies
 */
import React from 'react';
import { connect } from 'react-redux';
import { debounce, noop } from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';

/**
 * Internal dependencies
 */
import formSupport from 'services/form-support';
import Label from 'components/forms/label';
import Input from 'components/forms/input';
import Section from 'components/booking-form/section';
import SectionHeader from 'components/booking-form/section-header';
import StepController from '../step-controller';
import zoho from 'services/zoho';
import {
  handleDateChange,
  handleTextInputChange,
  setStepTitle,
  setCurrentStep,
} from 'state/booking-form/actions';


class ContactForm extends React.Component {
  constructor() {
    super();
    this.checkServiceArea = this.checkServiceArea.bind(this);
    this.validateInput = this.validateInput.bind(this);
    this.automaticZohoPost = this.automaticZohoPost.bind(this);
    this.debounceAutomaticZohoPost = debounce(this.automaticZohoPost, 3000);
  }

  componentDidMount() {
    // Set the date in our state before the date picker is loaded. If this doesn't happen here
    // it will cause errors when the date picker component loads later on.
    this.props.handleDate(moment().add(3, 'days'));
    formSupport.scrollToTop();
  }

  componentDidUpdate() {
    this.debounceAutomaticZohoPost();
  }

  checkServiceArea() {
    const { form } = this.props;

    if (this.props.form.zipCode === '78681') {
      document.getElementById('zipCode').classList.add('is-valid');
      document.getElementById('zipCode').classList.remove('is-invalid');

      return true;
    }
    document.getElementById('zipCode').classList.add('is-invalid');
    document.getElementById('zipCode').classList.remove('is-valid');

    return false;
  }

  validateInput() {
    const { form, setStep } = this.props;
    const formInputArray = [
      'firstName',
      'lastName',
      'mobileNumber',
      'emailAddress',
      'zipCode',
    ];
    const errorArray = [];

    formInputArray.forEach((inputName) => {
      document.getElementById(inputName).classList.remove('is-invalid');
      if (inputName === 'emailAddress' && !formSupport.isValidEmail(form.emailAddress)) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
      if (inputName === 'zipCode' && !this.checkServiceArea()) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
      if (inputName === 'mobileNumber' && !formSupport.isValidNAPhoneNumber(form.mobileNumber)) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
      if (form[inputName] === '' && errorArray.indexOf(inputName) === -1) {
        document.getElementById(inputName).classList.add('is-invalid');
        errorArray.push(inputName);
      }
    });

    if (errorArray.length === 0) {
      this.debounceAutomaticZohoPost();
      setStep();
    }
  }

  automaticZohoPost() {
    const { form } = this.props;
    const formInputArray = [
      'firstName',
      'lastName',
      'mobileNumber',
      'emailAddress',
      'zipCode',
    ];
    const isComplete = formInputArray.reduce((accu, stateVariable) => {
      if (form[stateVariable] === '') {
        return false;
      }
      return accu;
    }, true);

    if (isComplete) {
      zoho.upsertLead(form);
    }
  }

  render() {
    const { form, handleChange } = this.props;

    return (
      <form className="ContactForm">
        <div className="BookingForm-form_bg">
          <Section>
            <SectionHeader>How can we get in touch?</SectionHeader>
            <p className="BookingForm-section_desc col-12">Info for appointment reminders</p>
            <Label htmlFor="firstName" width="half">
              <span className="BookingForm-label_text">First Name</span>
              <Input htmlId="firstName" value={form.firstName} onChange={handleChange} placeholder="First Name" />
            </Label>
            <Label htmlFor="lastName" width="half">
              <span className="BookingForm-label_text">Last Name</span>
              <Input htmlId="lastName" value={form.lastName} onChange={handleChange} placeholder="Last Name" />
            </Label>
            <Label htmlFor="mobileNumber" width="full">
              <span className="BookingForm-label_text">Mobile Number*</span>
              <Input htmlId="mobileNumber" value={form.mobileNumber} onChange={handleChange} placeholder="Mobile Number" />
              <p className="BookingForm-input_desc">*Never forget an appointment with our automatic text notifications</p>
            </Label>
            <Label htmlFor="emailAddress" width="full">
              <span className="BookingForm-label_text">Email</span>
              <Input htmlId="emailAddress" value={form.emailAddress} onChange={handleChange} placeholder="Email" />
            </Label>
            <Label htmlFor="zipCode" className="col-5">
              <span className="BookingForm-label_text">Zip Code</span>
              <Input htmlId="zipCode" value={form.zipCode} onChange={handleChange} placeholder="Zip Code" />
            </Label>
            <div className="BookingForm-btn col-5">
              <button type="button" className="btn btn-primary" onClick={this.checkServiceArea}>Check Coverage</button>
            </div>
          </Section>
          <Section>
            <StepController text="Get Quote and Review Order" handleClick={this.validateInput} />
          </Section>
        </div>
      </form>
    );
  }
}

/**
 * Prop types
 */
ContactForm.propTypes = {
  handleDate: PropTypes.func,
  handleChange: PropTypes.func,
  currentStep: PropTypes.number,
  currentForm: PropTypes.string,
  form: PropTypes.object,
  setStep: PropTypes.func,
  setTitle: PropTypes.func,
  status: PropTypes.string,
};

/**
 * Default props
 */
ContactForm.defaultProps = {
  handleDate: noop,
  handleChange: noop,
  currentStep: 2,
  currentForm: '',
  form: {},
  setStep: noop,
  setTitle: noop,
  status: 'PENDING',
};

const mapStateToProps = state => ({
  ...state.bookingForm,
});

const mapDispatchToProps = dispatch => ({
  handleDate: date => dispatch(handleDateChange(date)),
  handleChange: event => dispatch(handleTextInputChange(event)),
  setStep: () => dispatch(setCurrentStep(3)),
  setTitle: title => (dispatch(setStepTitle(title))),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactForm);
