import React from 'react';

// Import layouts
import Body from 'components/page/body';

const NotFound = () => (
  <Body title="Maids Around Town - 404 Not Found">
    404: Not Found
  </Body>
);

export default NotFound;
