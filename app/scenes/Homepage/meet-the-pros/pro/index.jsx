import React from 'react';

const Pro = ({ imgUri, name }) => (
  <div className="MeetThePros-Pro">
    <img src={imgUri} alt={name} title={name} />
    <p>{name}</p>
  </div>
);

export default Pro;
