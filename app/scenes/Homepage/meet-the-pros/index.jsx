import React from 'react';

import Pro from './pro';

const MeetThePros = () => (
  <div className="Homepage-MeetThePros">
    <h2 className="Homepage-MeetThePros__title">Meet the Pros!</h2>
    <div className="MeetThePros">
      <Pro name="Alexandra" imgUri="/dist/images/alexandra.jpg" />
      <Pro name="Ashley" imgUri="/dist/images/ashley.jpg" />
      <Pro name="Irene" imgUri="/dist/images/irene.jpg" />
    </div>
  </div>
);

export default MeetThePros;
