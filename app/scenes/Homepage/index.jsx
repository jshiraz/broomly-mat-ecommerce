/**
 * External dependencies
 */
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import Helmet from 'react-helmet';

/**
 * Internal dependencies
 */
import getHeroImageHeight from 'services/hero-image';
import Body from 'components/page/body';
import HomepageServices from './homepage-services';
import Testimonials from './testimonials';
import MeetThePros from './meet-the-pros';

const Homepage = () => (
  <Body bodyClasses="Homepage">
    <Helmet>
      <title>Maids Around Town Austin - Homepage</title>
      <meta name="keywords" content="maids around town, book a cleaning, maid service austin" />
    </Helmet>
    <div className="Homepage-HeroImage" style={{ minHeight: getHeroImageHeight().toString().concat('px') }}>
      <div className="HeroImage-overlay" />
      <div className="HeroImage-content">
        <section>
          <h1>Let Us Take Care of Your Home.</h1>
          <p>Our Certified Home Service Professionals are ready to take care of your home.</p>
          <Link to="/book/residential"><button className="btn btn-primary">Book a Cleaning</button></Link>
        </section>
      </div>
    </div>
    <div className="Homepage-Services">
      <HomepageServices />
    </div>
    <div className="Homepage-Testimonials__bg">
      <Testimonials />
    </div>
    <div className="Homepage-MeetThePros__bg">
      <MeetThePros />
    </div>
  </Body>
);

export default withRouter(Homepage);
