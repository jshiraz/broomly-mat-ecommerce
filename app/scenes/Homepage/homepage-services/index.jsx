import React from 'react';

const HomepageServices = () => (
  <div className="HomepageServices">
    <section className="HomepageServices-box">
      <h2>Guaranteed Service</h2>
      <p>Satifaction guaranteed or we re-clean at no extra cost.</p>
      <img src="/dist/images/guaranteed_service.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
    </section>
    <section className="HomepageServices-box">
      <h2>Eco Friendly</h2>
      <p>We use all top-shelf eco-friendly supplies.</p>
      <img src="/dist/images/eco_friendly.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
    </section>
    <section className="HomepageServices-box">
      <h2>Top Cleaners</h2>
      <p>Our cleaners are thoroughly trained to provide you with a 5 star experience.</p>
      <img src="/dist/images/top_cleaners.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
    </section>
    <section className="HomepageServices-box">
      <h2>Online & Text Management</h2>
      <p>Simply text us for any questions or modifications regarding your appointment.</p>
      <img src="/dist/images/online_text_management.png" alt="Satisfaction guaranteed." title="Satisfaction guaranteed badge" />
    </section>
  </div>
);

export default HomepageServices;
