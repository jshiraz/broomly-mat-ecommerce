/**
 * External dependcies
 */
import React from 'react';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { StripeProvider } from 'react-stripe-elements';

/**
 * Internal dependencies
 */
import Loading from 'components/loading';
import { fetchMarketInfo } from 'state/market/actions';

/**
 * Loadable Components
 */
const Book = Loadable({
  loader: () => import('scenes/book'),
  loading: Loading,
});
const Homepage = Loadable({
  loader: () => import('scenes/homepage'),
  loading: Loading,
});
const NotFound = Loadable({
  loader: () => import('scenes/not-found'),
  loading: Loading,
});

class Router extends React.Component {
  constructor() {
    super();
    this.state = {
      stripe: null,
    };
  }

  componentDidMount() {
    const { fetchMarket } = this.props;
    fetchMarket();
    if (window.Stripe) {
      this.setState({ stripe: window.Stripe('pk_test_ElJavf4cxa9zMI4SE6ifRBxz') });
    } else {
      document.querySelector('#stripe-js').addEventListener('load', () => {
        // Create Stripe instance once Stripe.js loads
        this.setState({ stripe: window.Stripe('pk_test_ElJavf4cxa9zMI4SE6ifRBxz') });
      });
    }
  }

  render() {
    const { store } = this.props;
    return (
      <StripeProvider stripe={this.state.stripe}>
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Homepage} />
              <Route path="/book/:slug" component={Book} />
              <Route component={NotFound} />
            </Switch>
          </BrowserRouter>
        </Provider>
      </StripeProvider>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchMarket: () => dispatch(fetchMarketInfo()),
})

export default connect(null, mapDispatchToProps)(Router);
