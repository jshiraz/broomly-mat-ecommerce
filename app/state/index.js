/**
 * External dependencies
 */
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';

/**
 * Internal dependencies
 */
import market from './market/reducer';
import bookingForm from './booking-form/reducer';

export const reducer = combineReducers({
  market,
  bookingForm,
});

const middleware = [
  thunk,
  logger,
];


export const createReduxStore = () => (createStore(reducer, {}, applyMiddleware(...middleware)));
