/**
 * Internal dependencies
 */
import dummyData from 'services/dummydata';
import networkSim from 'services/network-sim';
import {
  FETCH_INITIAL_BOOKING_FORM_STATE,
  HANDLE_CHECKBOX_INPUT_TOGGLE,
  HANDLE_COUNTER_INPUT_DECREMENT,
  HANDLE_COUNTER_INPUT_INCREMENT,
  HANDLE_DATE_CHANGE,
  HANDLE_RADIO_INPUT_CHANGE,
  HANDLE_STRIPEID_INSERTION,
  HANDLE_TEXT_INPUT_CHANGE,
  SET_CLEANING_HOURS,
  SET_CURRENT_STEP,
  SET_STEP_TITLE,
} from '../action-types';

/**
 * The action creator for fetching the initial state.
 * @param {Object} initialState - the initial state that will load all the empty values
 * for the form.
 */
export const fetchInitialBookingFormStateAction = initialState => ({
  type: FETCH_INITIAL_BOOKING_FORM_STATE,
  status: 'SUCCESS',
  payload: initialState,
});

/**
 * Fetches the initial booking state to be used by the form inputs.
 * @param {String} slug - this is populated from a param passed down by React Router.
 */
export const fetchInitialBookingFormState = (slug) => {
  return (dispatch) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(dummyData.fetchForms);
      }, networkSim());
    })
      .then((forms) => {
        if (Array.isArray(forms)) {
          forms.forEach((form) => {
            if (form.slug === slug) {
              dispatch(fetchInitialBookingFormStateAction({
                form: form.reduxState,
                currentStep: 1,
                stepTitle: form.steps[0].title,
                currentForm: form.slug,
              }));
            }
          });
        }
      })
      .catch(() => {
        // Silently fail.
      });
  };
};

/**
 * Handles the value changes for text inputs.
 * @param {Object} event - the event information for the onChange of the given input.
 */
export const handleTextInputChange = event => ({
  type: HANDLE_TEXT_INPUT_CHANGE,
  payload: { [event.target.id]: event.target.value },
});

/**
 * Handles the value changes for radio buttons in forms.
 * @param {Object} event - the event information for the onChange of the given input.
 */
export const handleRadioInputChange = event => ({
  type: HANDLE_RADIO_INPUT_CHANGE,
  payload: { [event.target.name]: event.target.value },
});

/**
 * Handles the value changes for checkboxes in forms.
 * @param {Object} event - the event information for the onChange of the given input.
 */
export const handleCheckboxInputChange = event => ({
  type: HANDLE_CHECKBOX_INPUT_TOGGLE,
  payload: { [event.target.id]: event.target.checked },
});

/**
 * Handles a counter's number incrementing.
 * @param {Object} event - the event information for the onChange of the given input.
 */
export const handleCounterIncrementAction = (num, event) => ({
  type: HANDLE_COUNTER_INPUT_INCREMENT,
  payload: { [event.target.dataset.target]: num },
});

export const handleCounterIncrement = (max, step, event) => {
  return (dispatch) => {
    const incVal = (parseFloat(document.getElementById(event.target.dataset.target).value, 10) + parseFloat(step, 10));
    if (incVal > max) {
      dispatch(handleCounterIncrementAction(max, event));
      return null;
    }
    dispatch(handleCounterIncrementAction(incVal, event));
    return null;
  };
};

/**
 * Handles a counter's number decrementing.
 * @param {Object} event - the event information for the onChange of the given input.
 */
export const handleCounterDecrementAction = (num, event) => ({
  type: HANDLE_COUNTER_INPUT_DECREMENT,
  payload: { [event.target.dataset.target]: num },
});


export const handleCounterDecrement = (min, step, event) => {
  return (dispatch) => {
    const decVal = (parseFloat(document.getElementById(event.target.dataset.target).value, 10) - parseFloat(step, 10));
    if (decVal < min) {
      dispatch(handleCounterDecrementAction(min, event));
      return null;
    }
    dispatch(handleCounterDecrementAction(decVal, event));
    return null;
  };
};

/**
 * Takes the date provided by the ReactDatePicker and updates it in redux state.
 * @param {Object} date - moment.js date object.
 */
export const handleDateChange = date => ({
  type: HANDLE_DATE_CHANGE,
  payload: date,
});

export const handleStripeIdInsertion = stripeId => ({
  type: HANDLE_STRIPEID_INSERTION,
  payload: stripeId,
});

export const setCleaningHours = cleaningHours => ({
  type: SET_CLEANING_HOURS,
  payload: cleaningHours,
});

export const setCurrentStep = stepNum => ({
  type: SET_CURRENT_STEP,
  payload: stepNum,
});

/**
 * Sets the title that is on the booking page below the menu and above the progress bar.
 * @param {String} title - a page title.
 */
export const setStepTitle = title => ({
  type: SET_STEP_TITLE,
  payload: title,
});

