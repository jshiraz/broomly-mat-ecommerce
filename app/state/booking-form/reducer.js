/**
 * Internal dependencies
 */
import {
  FETCH_INITIAL_BOOKING_FORM_STATE,
  HANDLE_COUNTER_INPUT_DECREMENT,
  HANDLE_COUNTER_INPUT_INCREMENT,
  HANDLE_CHECKBOX_INPUT_TOGGLE,
  HANDLE_DATE_CHANGE,
  HANDLE_RADIO_INPUT_CHANGE,
  HANDLE_STRIPEID_INSERTION,
  HANDLE_TEXT_INPUT_CHANGE,
  SET_CLEANING_HOURS,
  SET_CURRENT_STEP,
  SET_STEP_TITLE,
} from 'state/action-types';

const initialState = {
  status: 'PENDING',
  // currentForm: '',
  // currentStep: 1,
  // form: {},
  // stepTitle: '',
};

const bookingForm = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_INITIAL_BOOKING_FORM_STATE:
      return {
        ...state,
        ...action.payload,
        status: action.status,
      };
    case HANDLE_COUNTER_INPUT_DECREMENT:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.payload,
        },
      };
    case HANDLE_COUNTER_INPUT_INCREMENT:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.payload,
        },
      };
    case HANDLE_CHECKBOX_INPUT_TOGGLE:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.payload,
        },
      };
    case HANDLE_DATE_CHANGE:
      return {
        ...state,
        form: {
          ...state.form,
          date: action.payload,
        },
      };
    case HANDLE_RADIO_INPUT_CHANGE:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.payload,
        },
      };
    case HANDLE_TEXT_INPUT_CHANGE:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.payload,
        },
      };
    case HANDLE_STRIPEID_INSERTION:
      return {
        ...state,
        form: {
          ...state.form,
          stripe: action.payload,
        },
      };
    case SET_CLEANING_HOURS:
      return {
        ...state,
        form: {
          ...state.form,
          cleaningHours: action.payload,
        },
      };
    case SET_CURRENT_STEP:
      return {
        ...state,
        currentStep: action.payload,
      };
    case SET_STEP_TITLE:
      return {
        ...state,
        stepTitle: action.payload,
      };
    default: return state;
  }
};

export default bookingForm;
