/**
 * Internal dependencies
 */
import dummyData from 'services/dummydata';
import networkSim from 'services/network-sim';

const formSupport = {};

/**
 * Calculates an estimate of how many cleaning hours will be required for the job.
 * @param {Object} form - form the redux form state passed to the function.
 */
formSupport.calculateCleaningHours = (form) => {
  const {
    bathrooms,
    bedrooms,
    insideFridge,
    insideCabinets,
    insideOven,
    interiorWalls,
    interiorWindows,
    sqft,
    washLaundry,
  } = form;
  const recHours = Math.ceil(((parseInt(bathrooms, 10) * 30) + (parseInt(bedrooms, 10) * 30) + (insideFridge ? 30 : 0) + (insideCabinets ? 30 : 0) + (insideOven ? 30 : 0) + (interiorWalls ? 60 : 0) + (interiorWindows ? 60 : 0)) / 60);

  if (recHours < 2) {
    return 2;
  }
  return recHours;
};

/**
 * Checks to see if a given email is valid.
 * @param {String} email - an email as a string
 */
formSupport.isValidEmail = (email) => {
  const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (typeof email !== 'undefined') {
    return regExp.test(email);
  }
  return false;
};

formSupport.isValidNAPhoneNumber = (phoneNumber) => {
  const regExp = /^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$/;
  if (typeof phoneNumber !== 'undefined') {
    return regExp.test(phoneNumber);
  }
  return false;
};

formSupport.isValidExpirationDate = (expiration) => {
  const regExp = /^([0-1][0-9])\/([0-9][0-9])$/;
  if (typeof expiration !== 'undefined') {
    return regExp.test(expiration);
  }
  return false;
};

formSupport.fetchForm = (slug, currentStep) => {
  return new Promise(resolve => (
    setTimeout(() => {
      dummyData.fetchForms.forEach((form) => {
        if (form.slug === slug) {
          form.steps.forEach((formStep) => {
            if (formStep.order === currentStep) {
              resolve(formStep);
            }
          });
        }
      });
    }, networkSim())
  ));
};

/**
 * Scrolls the window to the top of the page.
 * This is used because when you go to a new step the page doesn't reload.
 */
formSupport.scrollToTop = () => {
  if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
    window.scrollBy(0, -50);
    setTimeout(formSupport.scrollToTop, 10);
  }
};

export default formSupport;
