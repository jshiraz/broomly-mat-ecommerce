import axios from 'axios';
import moment from 'moment';
import dummyData from 'services/dummydata';

const zoho = {};

/**
 * Converts the string from the html values to the needed Zoho values.
 * @param {Object} form - the redux form state
 */
zoho.convertFrequency = (form) => {
  switch (form.frequency) {
    case 'once':
      return 'One-Time Other';
    case 'weekly':
      return 'Weekly';
    case 'biWeekly':
      return 'Biweekly';
    case 'monthly':
      return 'Monthly';
    default:
      return '-None-';
  }
};

zoho.getMarket = () => {
  const market = window.location.host.split('.')[0];

  if (market === 'localhost:3001' || market === 'www' || market === 'localhost:3000') {
    return 'Austin';
  }
  return market.charAt(0).toUpperCase() + market.slice(1);
};

/**
 * Formats the cleaning date and time to the proper Zoho CRM datatime format
 * @param {Object} date - a moment.js date object
 * @param {*} time - the time of the cleaning provided as a string
 */
zoho.formatDateTime = (date, time) => {
  let dateString;
  if (Object.keys(date).length === 0 && date.constructor === Object) {
    dateString = moment().add(3, 'days').format('MM/DD/YYYY');
  } else {
    dateString = date.format('MM/DD/YYYY');
  }
  return moment((dateString + ' ' + time), 'MM/DD/YYYY h:mmA');
};

/**
 * Calculates the next date that a cleaning will be performed.
 * @param {Object} form - the redux form state
 * @param {Object} cleaningDateTime - a moment.js object used for doing the next
 * cleaning calculation
 */
zoho.calcNextCleaning = (form, cleaningDateTime) => {
  if (form.frequency !== 'once') {
    switch (form.frequency) {
      case 'weekly':
        return cleaningDateTime.add(1, 'weeks');
      case 'biWeekly':
        return cleaningDateTime.add(2, 'weeks');
      case 'monthly':
        return cleaningDateTime.add(4, 'weeks');
      default:
        return null;
    }
  }
  return null;
};

zoho.getHourlyRate = (form) => {
  let priceRates;

  if (form.flex) {
    priceRates = dummyData.priceRates.flex;
  } else {
    priceRates = dummyData.priceRates.normal;
  }

  switch (form.frequency) {
    case 'once':
      return priceRates.once;
    case 'weekly':
      return priceRates.weekly;
    case 'biWeekly':
      return priceRates.biWeekly;
    case 'monthly':
      return priceRates.monthly;
    default:
      return '-None-';
  }
};

zoho.isRecurring = (form) => {
  if (form.frequency === 'once') {
    return false;
  }
  return true;
};

zoho.createZohoObject = (form) => {
  const initialObject = {
    Service_Type: ['Maid'],
    Market: zoho.getMarket(),
    Lead_Source: 'Commerce Website',
    First_Name: form.firstName,
    Last_Name: form.lastName,
    Phone: form.mobileNumber,
    Email: form.emailAddress,
    Zip_Code: form.zipCode,
    Street: form.address,
    Suite_Apt: form.aptSuite,
    City: form.city,
    State: form.state,
    Bedrooms: form.numBedrooms,
    Bathrooms: form.numBathrooms,
    Pets: form.typePets,
    Sq_Feet: parseInt(form.sqft, 10),
    Laundry: form.washLaundry,
    Fridge: form.insideFridge,
    Oven: form.insideOven,
    Cabinets: form.insideCabinets,
    Walls: form.interiorWalls,
    Windows: form.interiorWindows,
    Flex_Time: form.flex ? 'FULL Flex' : 'Exact Arrival',
    Supplies: 5,
    Frequency_of_Cleaning: zoho.convertFrequency(form),
    Total_Hours: form.cleaningHours,
    First_Cleaning: zoho.formatDateTime(form.date, form.arrivalTime).format(),
    Hourly_Rate: zoho.getHourlyRate(form),
  };

  if (zoho.isRecurring(form)) {
    const recurringObject = {
      Reoccurring_Next_Date_and_Time: zoho.calcNextCleaning(form, zoho.formatDateTime(form.date, form.arrivalTime)).format(),
      Recurring_Hourly_Rate: zoho.getHourlyRate(form),
      Recurring_Hours: form.cleaningHours,
      Recurring_Supplies: 5,
      Recurring_Discount: 0,
    };

    return Object.assign(recurringObject, initialObject);
  }

  return initialObject;
};

zoho.upsertLead = (form) => {
  return axios.post('/api/v1/zoho/lead', {
    zoho: zoho.createZohoObject(form),
    stripe: form.stripeId,
  });
};

zoho.convertToContact = (form) => {
  return axios.post('/api/v1/zoho/contact', {
    zoho: zoho.createZohoObject(form),
    stripe: form.stripe,
  });
};

export default zoho;
