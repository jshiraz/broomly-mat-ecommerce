const getHeroImageHeight = () => {
  const windowHeight = window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;
  return windowHeight - 64;
};

module.exports = getHeroImageHeight;
