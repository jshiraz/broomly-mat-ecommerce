const dummyData = {
  priceRates: {
    supplies: 5,
    taxRate: 0.14,
    flex: {
      once: 35,
      monthly: 30,
      biWeekly: 25,
      weekly: 22.5,
    },
    normal: {
      once: 40,
      monthly: 32.5,
      biWeekly: 27.5,
      weekly: 25,
    },
  },
  fetchMarketInfo: [
    {
      name: 'Austin',
      phoneNumber: '555-555-5594',
      email: 'test@email.com',
      coupons: {
        FLASHSALE: 0.2,
      },
      priceRates: {
        supplies: 5,
        taxRate: 0.14,
        flex: {
          once: 35,
          monthly: 30,
          biWeekly: 25,
          weekly: 22.5,
        },
        normal: {
          once: 40,
          monthly: 32.5,
          biWeekly: 27.5,
          weekly: 25,
        },
      },
    },
  ],
  fetchForms: [
    {
      formTitle: 'Residential',
      slug: 'residential',
      reduxState: {
        address: '',
        aptSuite: '',
        arrivalTime: '8:00AM',
        bathrooms: 1,
        bedrooms: 1,
        city: '',
        cleaningHours: 0,
        couponCode: '',
        couponDiscount: 0,
        date: {}, // moment().add(3, 'days')
        emailAddress: '',
        firstName: '',
        flex: false,
        frequency: 'once',
        insideFridge: false,
        insideCabinets: false,
        insideOven: false,
        interiorWalls: false,
        interiorWindows: false,
        lastName: '',
        mobileNumber: '',
        pets: 'No Pets',
        state: '',
        stripe: {},
        sqft: 500,
        washLaundry: false,
        zipCode: '',
      },
      steps: [
        {
          order: 1,
          title: 'Get a quote and book in 15 seconds!',
          sections: [
            {
              title: 'Tell Us About Your Home',
              description: null,
              buttonText: 'Get an Instant Online Pricing Estimate',
              fields: [
                {
                  htmlId: 'bedrooms',
                  name: {
                    singular: 'Bedroom',
                    plural: 'Bedrooms',
                  },
                  type: 'counter',
                  options: {
                    min: 0,
                    max: 10,
                  },
                  cols: 12,
                },
                {
                  htmlId: 'bathrooms',
                  name: {
                    singular: 'Bathroom',
                    plural: 'Bathrooms',
                  },
                  type: 'counter',
                  options: {
                    min: 0,
                    max: 10,
                  },
                  cols: 12,
                },
              ],
            },
            {
              title: null,
              description: null,
              fields: [
                {
                  htmlId: 'pets',
                  name: 'pets',
                  type: 'dropdown',
                  labelText: 'Do you have pets?',
                  options: {
                    selectValues: [
                      {
                        text: 'No Pets',
                        value: 'No Pets',
                      },
                      {
                        text: 'Cats',
                        value: 'Cats',
                      },
                      {
                        text: 'Dogs',
                        value: 'Dogs',
                      },
                      {
                        text: 'Others...',
                        value: 'Others',
                      },
                    ],
                  },
                  cols: 6,
                },
                {
                  htmlId: 'sqft',
                  name: 'sqft',
                  type: 'dropdown',
                  labelText: 'Square Footage',
                  options: {
                    selectValues: [
                      {
                        text: '500',
                        value: '500',
                      },
                      {
                        text: '1000',
                        value: '1000',
                      },
                      {
                        text: '1500',
                        value: '1500',
                      },
                      {
                        text: '2000',
                        value: '2000',
                      },
                      {
                        text: '2500+',
                        value: '2500',
                      },
                    ],
                  },
                  cols: 6,
                },
              ],
            },
            {
              title: 'Extras',
              description: null,
              fields: [
                {
                  htmlId: 'washLaundry',
                  name: 'washLaundry',
                  type: 'checkboxIcon',
                  labelText: 'Wash Laundry',
                  options: {
                    imageUri: '/dist/images/wash_laundry.svg',
                  },
                  cols: 4,
                },
                {
                  htmlId: 'insideFridge',
                  name: 'insideFridge',
                  type: 'checkboxIcon',
                  labelText: 'Inside Fridge',
                  options: {
                    imageUri: '/dist/images/inside_fridge.svg',
                  },
                  cols: 4,
                },
                {
                  htmlId: 'insideOven',
                  name: 'insideOven',
                  type: 'checkboxIcon',
                  labelText: 'Inside Oven',
                  options: {
                    imageUri: '/dist/images/inside_oven.svg',
                  },
                  cols: 4,
                },
                {
                  htmlId: 'insideCabinets',
                  name: 'insideCabinets',
                  type: 'checkboxIcon',
                  labelText: 'Inside Cabinets',
                  options: {
                    imageUri: '/dist/images/inside_cabinets.svg',
                  },
                  cols: 4,
                },
                {
                  htmlId: 'interiorWalls',
                  name: 'interiorWalls',
                  type: 'checkboxIcon',
                  labelText: 'Interior Walls',
                  options: {
                    imageUri: '/dist/images/interior_walls.svg',
                  },
                  cols: 4,
                },
                {
                  htmlId: 'interiorWindows',
                  name: 'interiorWindows',
                  type: 'checkboxIcon',
                  labelText: 'Interior Windows',
                  options: {
                    imageUri: '/dist/images/interior_windows.svg',
                  },
                  cols: 4,
                },
              ],
            },
          ],
        },
        // END INFO FORM
        {
          order: 2,
          title: 'Get a quote and book in 15 seconds!',
          sections: [
            {
              title: 'How can we get in touch?',
              description: 'Info for appointment reminders.',
              buttonText: 'Get Quote and Review Order',
              fields: [
                {
                  htmlId: 'firstName',
                  name: 'firstName',
                  type: 'text',
                  labelText: 'First Name',
                  options: {
                    placeholder: 'First Name',
                  },
                  cols: 6,
                },
                {
                  htmlId: 'lastName',
                  name: 'lastName',
                  type: 'text',
                  labelText: 'Last Name',
                  options: {
                    placeholder: 'Last Name',
                  },
                  cols: 6,
                },
                {
                  htmlId: 'mobileNumber',
                  name: 'mobileNumber',
                  type: 'text',
                  labelText: 'Mobile Number',
                  description: '*Never forget an appointment with our automatic text notifications!',
                  options: {
                    placeholder: '(555)-555-5555',
                  },
                  cols: 12,
                },
                {
                  htmlId: 'emailAddress',
                  name: 'emailAddress',
                  type: 'text',
                  labelText: 'Email',
                  options: {
                    placeholder: 'example@example.com',
                  },
                  cols: 12,
                },
                {
                  htmlId: 'zipCode',
                  name: 'zipCode',
                  type: 'zipCodeComponent',
                  labelText: 'Zip Code',
                  options: {
                    placeholder: 'Zip Code',
                  },
                  cols: 10,
                },
              ],
            },
          ],
        },
        {
          order: 3,
          title: 'Get a quote and book in 15 seconds!',
          sections: [
            {
              title: 'When can we help you?',
              description: null,
              buttonText: 'Get Quote and Review Order',
              fields: [
                {
                  htmlId: 'date',
                  name: 'date',
                  type: 'datePicker',
                  labelText: 'Preferred Date',
                  cols: 6,
                },
                {
                  htmlId: 'arrivalTime',
                  name: 'arrivalTime',
                  type: 'dropdown',
                  labelText: 'Preferred Arrival Time',
                  value: [
                    { text: '8:00AM', value: '8:00AM' },
                    { text: '8:30AM', value: '8:30AM' },
                    { text: '9:00AM', value: '9:00AM' },
                    { text: '9:30AM', value: '9:30AM' },
                    { text: '10:00AM', value: '10:00AM' },
                    { text: '10:30AM', value: '10:30AM' },
                    { text: '11:00AM', value: '11:00AM' },
                    { text: '11:30AM', value: '11:30AM' },
                    { text: '12:00PM', value: '12:00PM' },
                    { text: '12:30PM', value: '12:30PM' },
                    { text: '1:00PM', value: '1:00PM' },
                    { text: '1:30PM', value: '1:30PM' },
                    { text: '2:00PM', value: '2:00PM' },
                    { text: '2:30PM', value: '2:30PM' },
                    { text: '3:00PM', value: '3:00PM' },
                    { text: '3:30PM', value: '3:30PM' },
                    { text: '4:00PM', value: '4:00PM' },
                  ],
                  cols: 6,
                },
                {
                  htmlId: null,
                  name: null,
                  type: 'description',
                  labelText: 'We will check availability and confirm a date and time.',
                },
                {
                  htmlId: 'frequency',
                  name: 'frequency',
                  type: 'datePicker',
                  labelText: 'Preferred Date',
                  options: {},
                  cols: 12,
                },
                {
                  htmlId: 'flex',
                  name: 'flex',
                  type: 'checkbox',
                  labelText: 'Save with Flex!(8am - 4pm)',
                  cols: 6,
                },
                {
                  htmlId: null,
                  name: null,
                  type: 'flexWidget',
                },
                {
                  title: 'Price & Savings',
                  description: 'Recommended Time',
                  type: 'break',
                },
                {
                  htmlId: 'cleaningHours',
                  name: {
                    singular: 'Cleaning Hour',
                    plural: 'Cleaning Hours',
                  },
                  type: 'counter',
                  options: {
                    min: 2,
                    max: 32,
                  },
                  cols: 6,
                },
                {
                  htmlId: null,
                  name: null,
                  type: 'savingsWidget',
                  cols: 12,
                },
                {
                  htmlId: 'couponCode',
                  name: 'couponCode',
                  type: 'coupon',
                  labelText: '',
                  options: {
                    placeholder: 'Optional Coupon',
                  },
                  cols: 6,
                },
              ],
            },
          ],
        },
        {
          order: 4,
          title: 'Checkout',
          sections: [
            {
              title: 'When can we help you?',
              description: null,
              buttonText: 'Place Order',
              fields: [
                {
                  htmlId: 'firstName',
                  name: 'firstName',
                  type: 'text',
                  labelText: 'First Name',
                  cols: 6,
                },
                {
                  htmlId: 'lastName',
                  name: 'lastName',
                  type: 'text',
                  labelText: 'Last Name',
                  cols: 6,
                },
                {
                  htmlId: 'address',
                  name: 'address',
                  type: 'text',
                  labelText: 'Address',
                  cols: 8,
                },
                {
                  htmlId: 'aptSuite',
                  name: 'aptSuite',
                  type: 'text',
                  labelText: 'Apt/Suite',
                  cols: 4,
                },
                {
                  htmlId: 'city',
                  name: 'city',
                  type: 'text',
                  labelText: 'City',
                  cols: 5,
                },
                {
                  htmlId: 'state',
                  name: 'state',
                  type: 'text',
                  labelText: 'State',
                  cols: 3,
                },
                {
                  htmlId: 'zipCode',
                  name: 'zipCode',
                  type: 'text',
                  labelText: 'Zip Code',
                  cols: 4,
                },
                {
                  title: 'Payment',
                  description: 'Your credit card won&apos; be charged until after your appointment.',
                  type: 'break',
                },
              ],
            },
          ],
        },
      ],
    },
  ],
  bookingInfo: [
    {
      formTitle: 'Residential',
      slug: 'residential',
      submitButtonText: 'Get an Instant Online Pricing Estimate',
      formState: {
        numBedrooms: 2,
        numBathrooms: 1,
        typePets: 'nopets',
        sqft: '500',
        insideFridge: false,
        insideCabinets: false,
        insideOven: false,
        interiorWalls: false,
        interiorWindows: false,
        washLaundry: false,
      },
      sections: [
        {
          sectionTitle: 'Tell Us About Your Home',
          fields: [
            {
              htmlId: 'numBedrooms',
              fieldType: 'counter',
              max: 10,
              min: 0,
              fieldNameSingular: 'Bedroom',
              fieldNamePlural: 'Bedrooms',
            },
            {
              htmlId: 'numBathrooms',
              fieldType: 'counter',
              max: 10,
              min: 0,
              fieldNameSingular: 'Bathroom',
              fieldNamePlural: 'Bathrooms',
            },
          ],
        },
        {
          sectionTitle: null,
          fields: [
            {
              htmlId: 'typePets',
              fieldType: 'dropdown',
              fieldText: 'Do you have pets?',
              fieldOptions: [
                {
                  text: 'No Pets',
                  value: 'nopets',
                },
                {
                  text: 'Cats',
                  value: 'cats',
                },
                {
                  text: 'Dogs',
                  value: 'dogs',
                },
                {
                  text: 'Others...',
                  value: 'other',
                },
              ],
            },
            {
              htmlId: 'sqft',
              fieldType: 'dropdown',
              fieldText: 'Square Footage',
              fieldOptions: [
                {
                  text: '500',
                  value: '500',
                },
                {
                  text: '1000',
                  value: '1000',
                },
                {
                  text: '1500',
                  value: '1500',
                },
                {
                  text: '2000',
                  value: '2000',
                },
                {
                  text: '2500+',
                  value: '2500',
                },
              ],
            },
          ],
        },
        {
          sectionTitle: 'Extras',
          fields: [
            {
              htmlId: 'washLaundry',
              fieldType: 'checkboxIcon',
              fieldText: 'Wash Laundry',
              fieldImageUri: '/dist/images/wash_laundry.svg',
              fieldCheckedImageUri: '/dist/images/wash_laundry-green.svg',
            },
            {
              htmlId: 'insideFridge',
              fieldType: 'checkboxIcon',
              fieldText: 'Inside Fridge',
              fieldImageUri: '/dist/images/inside_fridge.svg',
              fieldCheckedImageUri: '/dist/images/inside_fridge-green.svg',
            },
            {
              htmlId: 'insideOven',
              fieldType: 'checkboxIcon',
              fieldText: 'Inside Oven',
              fieldImageUri: '/dist/images/inside_oven.svg',
              fieldCheckedImageUri: '/dist/images/inside_oven-green.svg',
            },
            {
              htmlId: 'insideCabinets',
              fieldType: 'checkboxIcon',
              fieldText: 'Inside Cabinets',
              fieldImageUri: '/dist/images/inside_cabinets.svg',
              fieldCheckedImageUri: '/dist/images/inside_cabinets-green.svg',
            },
            {
              htmlId: 'interiorWalls',
              fieldType: 'checkboxIcon',
              fieldText: 'Interior Walls',
              fieldImageUri: '/dist/images/interior_walls.svg',
              fieldCheckedImageUri: '/dist/images/interior_walls-green.svg',
            },
            {
              htmlId: 'interiorWindows',
              fieldType: 'checkboxIcon',
              fieldText: 'Interior Windows',
              fieldImageUri: '/dist/images/interior_windows.svg',
              fieldCheckedImageUri: '/dist/images/interior_windows-green.svg',
            },
          ],
        },
      ],
    },
    {
      formTitle: 'Offices',
      slug: 'offices',
      submitButtonText: 'Get an Instant Online Pricing Estimate',
      formState: {
        numOffices: 1,
        numBathrooms: 1,
        sqft: '500-100',
        buildingRetail: false,
        buildingCommercial: false,
        buildingOfficeSuite: false,
        buildingHomeOffice: false,
        buildingWarehouse: false,
        buildingOther: false,
      },
      sections: [
        {
          sectionTitle: 'Tell Us About Your Office',
          fields: [
            {
              htmlId: 'numOffices',
              fieldType: 'counter',
              max: 10,
              min: 0,
              fieldNameSingular: 'Office',
              fieldNamePlural: 'Offices',
            },
            {
              htmlId: 'numBathrooms',
              fieldType: 'counter',
              max: 10,
              min: 0,
              fieldNameSingular: 'Bathroom',
              fieldNamePlural: 'Bathrooms',
            },
            {
              htmlId: 'sqft',
              fieldType: 'dropdown',
              fieldText: 'Square Footage',
              fieldOptions: [
                {
                  text: '500-1000',
                  value: '500-100',
                },
                {
                  text: '1000-2500',
                  value: '1000-2500',
                },
                {
                  text: '2500+',
                  value: '2500+',
                },
              ],
            },
          ],
        },
        {
          sectionTitle: 'Office Type',
          fields: [
            {
              htmlId: 'buildingRetail',
              fieldType: 'checkboxIcon',
              fieldText: 'Retail',
              fieldImageUri: '/dist/images/retail.svg',
              fieldCheckedImageUri: '/dist/images/retail-green.svg',
            },
            {
              htmlId: 'buildingCommercial',
              fieldType: 'checkboxIcon',
              fieldText: 'Commercial Building',
              fieldImageUri: '/dist/images/commercial_building.svg',
              fieldCheckedImageUri: '/dist/images/commercial_building-green.svg',
            },
            {
              htmlId: 'buildingOfficeSuite',
              fieldType: 'checkboxIcon',
              fieldText: 'Office Suite',
              fieldImageUri: '/dist/images/office_suite.svg',
              fieldCheckedImageUri: '/dist/images/office_suite-green.svg',
            },
            {
              htmlId: 'buildingHomeOffice',
              fieldType: 'checkboxIcon',
              fieldText: 'Commercial Building',
              fieldImageUri: '/dist/images/commercial_building.svg',
              fieldCheckedImageUri: '/dist/images/commercial_building-green.svg',
            },
            {
              htmlId: 'buildingWarehouse',
              fieldType: 'checkboxIcon',
              fieldText: 'Warehouse',
              fieldImageUri: '/dist/images/warehouse.svg',
              fieldCheckedImageUri: '/dist/images/warehouse-green.svg',
            },
            {
              htmlId: 'buildingOther',
              fieldType: 'checkboxIcon',
              fieldText: 'Other',
              fieldImageUri: '/dist/images/other.svg',
              fieldCheckedImageUri: '/dist/images/other-green.svg',
            },
          ],
        },
      ],
    },
  ],
};

module.exports = dummyData;
