exports.up = function(knex, Promise) {
  return Promise.all([
    // Create the locations table.
    knex.schema.createTable('locations', (table) => {
      table.increments('location_id').primary();
      table.string('location_name', 128);
      table.string('location_slug', 128);
      table.string('location_phonenumber', 64);
      table.string('location_email', 128);
    }),
    // Create the zoho table.
    knex.schema.createTable('zoho', (table) => {
      table.string('zoho_oauth_token', 128);
      table.string('zoho_refresh_token', 128);
      table.string('zoho_client_id', 128);
      table.string('last_refreshed', 64);
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('locations'),
    knex.schema.dropTable('zoho'),
  ]);
};
